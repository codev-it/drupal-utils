// noinspection JSCheckFunctionSignatures

/**
 * @file
 * Drupal reload ajax command.
 *
 * This file provides a custom AJAX command in Drupal, named 'reload'.
 * The command is used to reload the current page and optionally display
 * a throbber (loading indicator) while the page is reloading. The type of
 * throbber and its placement can be controlled through the AJAX response.
 *
 * @see Drupal.AjaxCommands
 * @see window.location
 */

(($, Drupal) => {
  /**
   * Custom AJAX command to reload the current page with optional throbber.
   *
   * This command allows the reloading of the current page and can display a
   * throbber while the page is reloading. The throbber can be either attached
   * to the triggering element or shown fullscreen.
   *
   * Usage:
   * This command can be invoked in a Drupal AJAX response using the following
   * syntax:
   * @code
   *   $response->addCommand(new \Drupal\Core\Ajax\InvokeCommand(
   *     null,
   *     'reload',
   *     [['throbber', 'fullscreen']]
   *   ));
   * @endcode
   *
   * @param {Drupal.Ajax} ajax - The Drupal AJAX instance.
   * @param {Object} response - The AJAX response object.
   * @param {string} response.throbber - The type of throbber to display
   *   ('throbber' or 'fullscreen').
   * @param {HTMLElement} response.triggeringElement - The element that
   *   triggered the AJAX request.
   */
  Drupal.AjaxCommands.prototype.reload = (ajax, response) => {
    let { throbber } = response;
    const { triggeringElement } = response;

    if (throbber.length) {
      const $body = $('body');
      const $elem = $(triggeringElement);

      if (throbber === 'throbber' && $elem.length === 0) {
        throbber = 'fullscreen';
      }

      switch (throbber) {
        case 'throbber':
          $elem.append($(Drupal.theme('ajaxProgressThrobber')));
          break;
        case 'fullscreen':
          $body.append($(Drupal.theme('ajaxProgressIndicatorFullscreen')));
          break;
        default:
      }
    }
    window.location.reload();
  };
})(jQuery, Drupal);
