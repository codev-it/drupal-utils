// noinspection JSUnusedGlobalSymbols

/**
 * @file
 * Implements an AJAX command to scroll the Drupal dialog to the top.
 *
 * This file provides a Drupal AJAX command that can be used to scroll
 * the content of a modal dialog to the top. This is particularly useful
 * in scenarios where the dialog content changes and needs to be reset to
 * the beginning. The command is implemented as a method of the
 * Drupal.AjaxCommands prototype.
 *
 * @see Drupal.AjaxCommands
 * @see jQuery
 */

(function($, Drupal) {
  /**
   * Adds a custom command to Drupal's AJAX framework for scrolling the modal
   * dialog.
   *
   * This command, 'dialogScrollTop', targets the modal dialog with the ID
   * 'drupal-modal' and sets its scroll position to the top. The command is
   * added to Drupal's AJAX framework and can be invoked from server-side AJAX
   * responses to reset the scroll position of the modal.
   *
   * Usage:
   * This command can be invoked in a Drupal AJAX response using the following
   * syntax:
   * @code
   *   $response->addCommand(new
   *   \Drupal\Core\Ajax\InvokeCommand('#drupal-modal', 'dialogScrollTop'));
   * @endcode
   */
  Drupal.AjaxCommands.prototype.dialogScrollTop = function() {
    // Scroll the modal dialog with ID 'drupal-modal' to the top.
    // The scrollTop method sets the vertical position of the scroll bar for
    // the selected element.
    $('#drupal-modal').scrollTop(0);
  };
})(jQuery, Drupal);
