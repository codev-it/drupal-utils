// noinspection JSUnusedGlobalSymbols

/**
 * @file
 * Drupal replace page title command.
 *
 * This file provides a custom AJAX command in Drupal, named
 *   'replacePageTitleCommand'. The command is used to replace the content of
 *   the page title on a Drupal site. It targets an element with the
 *   'data-drupal-page-title' attribute and updates its content with the new
 *   title provided in the AJAX response.
 *
 * @see Drupal.AjaxCommands
 * @see jQuery
 */

(function($, Drupal) {
  /**
   * Custom AJAX command to replace the page title.
   *
   * This command updates the content of the page title by selecting the
   * element
   * with the 'data-drupal-page-title' attribute and replacing its HTML content
   * with the new title provided in the AJAX response. If the response does not
   * include a title, the page title content is set to an empty string.
   *
   * Usage:
   * This command can be invoked in a Drupal AJAX response using the following
   * syntax:
   * @code
   *   $response->addCommand(new \Drupal\Core\Ajax\InvokeCommand(
   *     null,
   *     'replacePageTitleCommand',
   *     ['title' => 'New Page Title']
   *   ));
   * @endcode
   *
   * @param {Drupal.Ajax} ajax - The Drupal AJAX instance.
   * @param {Object} response - The AJAX response object.
   * @param {string} response.title - The new title for the page.
   */
  Drupal.AjaxCommands.prototype.replacePageTitleCommand = (ajax, response) => {
    $('[data-drupal-page-title]').html(response.title || '');
  };
})(jQuery, Drupal);
