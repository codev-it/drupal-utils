// noinspection All,ES6ConvertVarToLetConst

/**
 * @file
 * Drupal browser history update command.
 *
 * This file provides a custom AJAX command in Drupal to update the browser's
 * history. It is designed to work with the browser's navigation (back and
 *   forward buttons) by handling the 'popstate' event. This implementation
 *   allows dynamic content updates while maintaining correct browser history
 *   navigation.
 *
 * @see Drupal.AjaxCommands
 * @see window.history
 * @see window.location
 */

(function ($, Drupal, window, document) {
  var AjaxCommands = Drupal.AjaxCommands;
  var history = window.history,
    location = window.location;

  /**
   * Handles the popstate event to reload history data.
   *
   * This function is triggered when the user navigates using the browser's
   * back or forward buttons. It checks if the state object contains a
   * 'selector', and uses AJAX to reload the content for that selector. If the
   * state object is empty, the page is reloaded.
   *
   * @param {PopStateEvent} e - The popstate event object.
   */
  window.onpopstate = function (e) {
    if (e.state && e.state.selector) {
      var selector = e.state.selector;
      var ajaxReq = Drupal.ajax({
        url: e.target.location.href
      });
      ajaxReq.commands.insert = function (ajax, response) {
        $(selector).replaceWith(response.data);
      };
      ajaxReq.execute();
    } else {
      location.reload();
    }
  };

  // noinspection JSUnusedGlobalSymbols
  /**
   * Custom AJAX command to update browser history.
   *
   * This command updates the browser's history using the History API.
   * It can be invoked from a server-side AJAX response to change the URL
   * without reloading the page, while still updating the browser's history.
   *
   * @param {Drupal.Ajax} ajax - The Drupal AJAX instance.
   * @param {Object} response - The AJAX response object.
   * @param {string} response.title - The new title for the page.
   * @param {string} response.url - The new URL to push to the browser's
   *   history.
   * @param {Object} response.data - The state object to associate with the new
   *   history entry.
   */
  AjaxCommands.prototype.historyUpdate = function (ajax, response) {
    var title = response.title,
      url = response.url,
      data = response.data;
    url && history.pushState(data || {}, title || document.title, url);
  };
})(jQuery, Drupal, window, document);
