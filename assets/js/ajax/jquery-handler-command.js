// noinspection All

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }
function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }
function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
/**
 * @file
 * Drupal reload ajax command.
 *
 * This file provides a custom AJAX command in Drupal, named 'jQueryHandler'.
 * This command allows the execution of jQuery methods on elements specified
 * by a selector. It is flexible and can handle various jQuery methods and
 * settings, making it a versatile tool for updating the DOM in response to
 * AJAX requests.
 *
 * @see Drupal.AjaxCommands
 * @see jQuery
 */

(function ($, Drupal) {
  var AjaxCommands = Drupal.AjaxCommands;

  /**
   * Custom AJAX command for handling jQuery methods.
   *
   * This command applies specified jQuery methods to elements matched by the
   * provided selector. It can handle different types of settings, including
   * strings and objects, and execute the corresponding jQuery method.
   *
   * Usage:
   * This command can be invoked in a Drupal AJAX response using the following
   * syntax:
   * @code
   *   $response->addCommand(new \Drupal\Core\Ajax\InvokeCommand(
   *     '[selector]',
   *     'jQueryHandler',
   *     [['method', [settings]]]
   *   ));
   * @endcode
   *
   * @param {Drupal.Ajax} ajax - The Drupal AJAX instance.
   * @param {Object} response - The AJAX response object.
   * @param {string} response.selector - The jQuery selector to target
   *   elements.
   * @param {string} response.method - The jQuery method to apply.
   * @param {string|Object} response.settings - Optional settings to pass to
   *   the jQuery method.
   */
  AjaxCommands.prototype.jQueryHandler = function (ajax, response) {
    var selector = response.selector,
      method = response.method,
      settings = response.settings;
    var $elem = $(selector);
    if ($elem.length && $elem[method] !== undefined) {
      switch (_typeof(settings || undefined)) {
        case 'string':
          $elem[method](settings);
          break;
        case 'object':
          $elem[method].apply($elem, _toConsumableArray(settings));
          break;
        default:
          $elem[method]();
      }
    }
  };
})(jQuery, Drupal);
