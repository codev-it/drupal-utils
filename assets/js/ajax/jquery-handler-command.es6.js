// noinspection JSUnusedGlobalSymbols

/**
 * @file
 * Drupal reload ajax command.
 *
 * This file provides a custom AJAX command in Drupal, named 'jQueryHandler'.
 * This command allows the execution of jQuery methods on elements specified
 * by a selector. It is flexible and can handle various jQuery methods and
 * settings, making it a versatile tool for updating the DOM in response to
 * AJAX requests.
 *
 * @see Drupal.AjaxCommands
 * @see jQuery
 */

(($, Drupal) => {
  const { AjaxCommands } = Drupal;

  /**
   * Custom AJAX command for handling jQuery methods.
   *
   * This command applies specified jQuery methods to elements matched by the
   * provided selector. It can handle different types of settings, including
   * strings and objects, and execute the corresponding jQuery method.
   *
   * Usage:
   * This command can be invoked in a Drupal AJAX response using the following
   * syntax:
   * @code
   *   $response->addCommand(new \Drupal\Core\Ajax\InvokeCommand(
   *     '[selector]',
   *     'jQueryHandler',
   *     [['method', [settings]]]
   *   ));
   * @endcode
   *
   * @param {Drupal.Ajax} ajax - The Drupal AJAX instance.
   * @param {Object} response - The AJAX response object.
   * @param {string} response.selector - The jQuery selector to target
   *   elements.
   * @param {string} response.method - The jQuery method to apply.
   * @param {string|Object} response.settings - Optional settings to pass to
   *   the jQuery method.
   */
  AjaxCommands.prototype.jQueryHandler = (ajax, response) => {
    const { selector, method, settings } = response;
    const $elem = $(selector);
    if ($elem.length && $elem[method] !== undefined) {
      switch (typeof (settings || undefined)) {
        case 'string':
          $elem[method](settings);
          break;
        case 'object':
          $elem[method](...settings);
          break;
        default:
          $elem[method]();
      }
    }
  };
})(jQuery, Drupal);
