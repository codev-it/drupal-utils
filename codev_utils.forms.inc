<?php

/**
 * @file
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

use Drupal\codev_utils\Ajax\ReloadCommand;
use Drupal\codev_utils\Helper\User as UserHelper;
use Drupal\codev_utils\Helper\Utils;
use Drupal\codev_utils\Settings;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\views\ViewExecutable;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implements hook_form_alter().
 *
 * Alter for user forms.
 *
 * @noinspection PhpUnused
 */
function codev_utils_form_alter(array &$form, FormStateInterface $form_state, $form_id): void {
  switch ($form_id) {
    case 'user_login_form':
    case 'user_register_form':
    case 'user_pass':
      $form['#attached']['library'][] = 'codev_utils/ajax.reload-command';

      $request = Drupal::request();
      $is_ajax = $request->isXmlHttpRequest();
      if ($is_ajax) {
        $form_state->disableRedirect();
        $form['#submit'][] = '_codev_utils_disable_redirect_form_submit';
        $form['actions']['submit']['#ajax']['wrapper'] = $form['#id'];
        $form['actions']['submit']['#ajax']['progress'] = Settings::get('login_ajax_throbber');
        $form['actions']['submit']['#ajax']['callback'] = '_codev_utils_form_reload_callback';

        switch ($form_id) {
          case 'user_login_form':
            $form['#validate'][] = '_codev_utils_user_login_ajax_response_validate';
            if (!empty($form['actions']['#suffix'])) {
              $url = Url::fromRoute(Settings::USER_REGISTER_ROUTE);
              $attr = $url->getOption('attributes') ?? [];
              Utils::appendDialogModalAttr($attr);
              $url->setOption('attributes', $attr);
              $link = Link::fromTextAndUrl(t('Create new account'), $url);
              $form['actions']['#suffix'] = sprintf(
                '<div class="actions-suffix">%s</div>', $link->toString());
            }
            if (!empty($form['reset'])) {
              $url = Url::fromRoute(Settings::USER_PASS_RESET_ROUTE);
              $attr = $url->getOption('attributes') ?? [];
              Utils::appendDialogModalAttr($attr);
              $url->setOption('attributes', $attr);
              $link = Link::fromTextAndUrl(t('Forgot Password?'), $url);
              $form['reset']['#markup'] = sprintf(
                '<p class="forgot-password">%s</p>', $link->toString());
            }
            break;
          case 'user_register_form':
          case 'user_pass':
            if (!empty($form['reset'])) {
              $url = Url::fromRoute(Settings::USER_LOGIN_ROUTE);
              $attr = $url->getOption('attributes') ?? [];
              Utils::appendDialogModalAttr($attr);
              $url->setOption('attributes', $attr);
              $link = Link::fromTextAndUrl(t('Go back to the login page.'), $url);
              $form['reset']['#markup'] = sprintf(
                '<p class="back-to-login">%s</p>', $link->toString());
            }
            break;
        }
      }
      break;
  }

  // Check for roles access
  if ($form_id == 'user_form' || $form_id == 'user_register_form') {
    $user = Drupal::currentUser();
    if (!$user->hasPermission(Settings::ACCESS_KEY_PERMISSION_ADMIN)) {
      if (!empty($form['account']['roles'])) {
        $form['#validate'][] = '_codev_utils_user_register_form_roles_validate';
        $accept_roles = array_keys(UserHelper::getAllRoles(TRUE));
        $options = $form['account']['roles']['#options'];
        foreach ($options as $key => $val) {
          if (!in_array($key, $accept_roles)) {
            unset($form['account']['roles']['#options'][$key]);
          }
        }
      }
    }
  }
}

/**
 * Check for flood bocked validation.
 *
 * @noinspection PhpUnused
 */
function _codev_utils_user_login_ajax_response_validate(array $form, FormStateInterface $form_state): void {
  $response = $form_state->getResponse();
  if ($response instanceof Response
    && $response->getStatusCode() >= 300
    && !empty($response->getContent())
    && !str_contains($response->getContent(), $form_state->getValue('pass'))) {
    $form_state->setError($form['name'], Markup::create($response->getContent()));
  }
}

/**
 * Disable redirect form submit handler.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_utils_disable_redirect_form_submit(array $form, FormStateInterface $form_state): void {
  $form_state->disableRedirect();
}

/**
 * Reload the page if the form submit successes without errors.
 *
 * @noinspection PhpUnused
 */
function _codev_utils_form_reload_callback(array $form, FormStateInterface $form_state): AjaxResponse|array {
  $form_state->setRebuild();
  if (!$form_state->hasAnyErrors()) {
    $response = new AjaxResponse();
    /** @noinspection SpellCheckingInspection */
    $response->addCommand(new ReloadCommand(
      Settings::get('login_ajax_throbber'),
      '.ui-dialog .ui-dialog-buttonpane .ui-dialog-buttonset',
    ));
    return $response;
  }
  return $form;
}

/**
 * Check for user role access form validation.
 *
 * @noinspection PhpUnused
 */
function _codev_utils_user_register_form_roles_validate(array $form, FormStateInterface $form_state): void {
  $roles = $form_state->getValue('roles');
  if (!empty($roles)) {
    $accept_roles = array_keys(UserHelper::getAllRoles(TRUE));
    foreach ($roles as $role) {
      if (!in_array($role, $accept_roles)) {
        $elem = $form['account']['roles'];
        $form_state->setError($elem, t('Invalid input'));
      }
    }
  }
}

/**
 * Implements hook_views_exposed_form().
 *
 * @noinspection PhpUnused
 */
function codev_utils_form_views_exposed_form_alter(array &$form, FormStateInterface $form_state): void {
  $storage = $form_state->getStorage();
  if ($view = Utils::getArrayValue('view', $storage) ?: NULL) {
    /** @var ViewExecutable $view */
    if ($view->id() == 'user_admin_people' && !empty($form['permission'])) {
      $user = Drupal::currentUser();
      $options = $form['permission']['#options'];
      foreach ($options as $key => $option) {
        if (is_array($option)) {
          foreach ($option as $perm => $desc) {
            if (!$user->hasPermission($perm)) {
              unset($form['permission']['#options'][$key][$perm]);
            }
          }
          if (empty($form['permission']['#options'][$key])) {
            unset($form['permission']['#options'][$key]);
          }
        }
      }
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for user_admin_permissions.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_utils_form_user_admin_permissions_alter(array &$form, FormStateInterface $form_state): void {
  $user = Drupal::currentUser();
  if (!$user->hasPermission(Settings::ACCESS_KEY_PERMISSION_ADMIN)
    && !empty($form['permissions'])) {
    $last_permission = '';
    $permissions = $form['permissions'];
    $roles = array_keys(UserHelper::getAllRoles(FALSE, FALSE, $user));
    foreach ($permissions as $permission => $item) {
      if ($permission[0] != '#') {
        if (!empty($item[RoleInterface::ANONYMOUS_ID])) {
          if (!$user->hasPermission($permission)) {
            unset($form['permissions'][$permission]);
          }
          else {
            foreach ($form['permissions'][$permission] as $role_id => $data) {
              if (!in_array($role_id, $roles)) {
                $form['permissions'][$permission][$role_id]['#disabled'] = TRUE;
              }
            }
            $last_permission = $permission;
          }
        }
        else {
          if (!empty($permissions[$last_permission])
            && empty($permissions[$last_permission][RoleInterface::ANONYMOUS_ID])) {
            unset($form['permissions'][$last_permission]);
          }
          $last_permission = $permission;
        }
      }
    }
    if (!empty($permissions[$last_permission])
      && empty($permissions[$last_permission][RoleInterface::ANONYMOUS_ID])) {
      unset($form['permissions'][$last_permission]);
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for user_admin_roles_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_utils_form_user_admin_roles_form_alter(array &$form, FormStateInterface $form_state): void {
  $user = Drupal::currentUser();
  $roles = array_keys(UserHelper::getAllRoles(FALSE, FALSE, $user));
  if (!$user->hasPermission(Settings::ACCESS_KEY_PERMISSION_ADMIN)) {
    $form['#validate'][] = '_codev_utils_form_user_admin_roles_form_validate';
    $entries = Utils::getArrayValue('entities', $form, []);
    foreach ($entries as $key => $entry) {
      if ($key[0] != '#' && !in_array($key, $roles)) {
        unset($form['entities'][$key]);
      }
    }
  }
}

/**
 * User admin roles form validator.
 *
 * @noinspection PhpUnused
 */
function _codev_utils_form_user_admin_roles_form_validate(array $form, FormStateInterface $form_state): void {
  try {
    $weight_delta = intval('-' . $form['entities'][RoleInterface::ANONYMOUS_ID]['weight']['#delta']);
    $entities = $form_state->getValue('entities');
    /** @var Role[] $roles */
    $roles = Drupal::entityTypeManager()
      ->getStorage('user_role')
      ->loadMultiple();
    Utils::sortObjectByMethod($roles);
    Utils::sortArrayByKey($entities);
    foreach ($entities as &$entity) {
      $entity['weight'] = $weight_delta;
      $weight_delta++;
    }
    foreach ($roles as $role) {
      if (empty($entities[$role->id()])) {
        $entities[$role->id()] = ['weight' => $weight_delta];
        $weight_delta++;
      }
    }
    $form_state->setValue('entities', $entities);
  } catch (Exception) {

  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for user_roles_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_utils_form_user_role_form_alter(array &$form, FormStateInterface $form_state): void {
  $user = Drupal::currentUser();
  if (empty($form['id']['#default_value'])
    && !$user->hasPermission(Settings::ACCESS_KEY_PERMISSION_ADMIN)) {
    $form['#validate'][] = '_codev_utils_form_user_role_form_validate';
  }
}

/**
 * User role form submit.
 *
 * @param array              $form
 * @param FormStateInterface $form_state
 *
 * @noinspection PhpUnusedParameterInspection
 * @noinspection PhpUnused
 */
function _codev_utils_form_user_role_form_validate(array $form, FormStateInterface $form_state): void {
  $user_roles = UserHelper::getAllRoles(FALSE, TRUE);
  /** @var Role $last_role */
  $last_role = end($user_roles);
  $form_state->setValue('weight', $last_role->getWeight());
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for block_admin_display_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_utils_form_block_form_alter(array &$form, FormStateInterface $form_state): void {
  $form['actions']['submit']['#submit'][] = '_codev_utils_form_block_form_submit';
}

/**
 * Block form submit.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_utils_form_block_form_submit(array $form, FormStateInterface $form_state): void {
  /** @var Url $redirect */
  $redirect = $form_state->getRedirect();
  if (!empty($params = $redirect->getRouteParameters())) {
    $user = Drupal::currentUser();
    $theme = Utils::getArrayValue('theme', $params, '');
    $perm_key = sprintf(Settings::ACCESS_KEY_THEME, $theme);
    if ($user->hasPermission(Settings::ACCESS_KEY_BLOCKS)
      && !$user->hasPermission($perm_key)) {
      $form_state->setRedirect('block.admin_display');
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for block_admin_display_form.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function codev_utils_form_system_cron_settings_alter(array &$form, FormStateInterface $form_state): void {
  $settings = Settings::toArray();
  $form['remove_unused_images'] = [
    '#type'                         => 'details',
    '#title'                        => t('Images'),
    'remove_unused_images'          => [
      '#type'          => 'checkbox',
      '#title'         => t('Remove unused images on cron run.'),
      '#default_value' => $settings['cron_remove_unused_images'] ?? FALSE,
    ],
    'remove_unused_images_interval' => [
      '#type'          => 'select',
      '#title'         => t('Remove unused images interval.'),
      '#default_value' => $settings['cron_remove_unused_images_interval'] ?? NULL,
      '#options'       => [
        '+1 day'  => t('Daily'),
        '+7 day'  => t('Weekly'),
        '+30 day' => t('Monthly'),
      ],
      '#states'        => [
        'visible' => [
          ':input[name="remove_unused_images"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ],
    'generate_image_styles'         => [
      '#type'          => 'checkbox',
      '#title'         => t('Enable cron generate image styles.'),
      '#descrition'    => t("This is helpful if you do not manage images via the standard interface. (Migrations, API's ...)"),
      '#default_value' => $settings['cron_generate_images_styles'] ?? FALSE,
    ],
  ];
  $form['#submit'][] = '_codev_utils_form_system_cron_settings_submit';
}

/**
 * System cron settings form submit.
 *
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */
function _codev_utils_form_system_cron_settings_submit(array $form, FormStateInterface $form_state): void {
  $settings = Settings::getEditable();
  $settings->set('cron_remove_unused_images', $form_state->getValue('remove_unused_images'))
    ->set('cron_remove_unused_images_interval', $form_state->getValue('remove_unused_images_interval'))
    ->set('cron_generate_images_styles', $form_state->getValue('generate_image_styles'))
    ->save();
}
