<?php

/**
 * @file
 * Contains the RouteSubscriber class to modify routes dynamically in the
 * Drupal Codev-IT project.
 *
 * This file provides the RouteSubscriber class, responsible for listening to
 * dynamic route events and modifying routes based on certain conditions. It
 * includes methods to add permissions to routes.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Routing;

use Drupal\codev_utils\Settings;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 *
 * This class extends RouteSubscriberBase to modify routes dynamically, such as
 * adding permissions to certain routes based on specific access keys.
 *
 * @noinspection PhpUnused
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * Alters routes during the route building process.
   *
   * Modifies routes by adding custom permissions to them. This is done by
   * fetching the route from the collection and invoking addPermissionToRoute
   * method with the required permissions.
   *
   * @param RouteCollection $collection
   *   The collection of all routes.
   */
  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('system.status')) {
      $this->addPermissionToRoute($route, Settings::ACCESS_KEY_VIEW_STATUS_REPORT);
    }

    if ($route = $collection->get('system.site_information_settings')) {
      $this->addPermissionToRoute($route, Settings::ACCESS_KEY_BASIC_SETTINGS);
    }

    if ($route = $collection->get('admin_toolbar_tools.flush')) {
      $this->addPermissionToRoute($route, Settings::ACCESS_KEY_CACHE);
    }

    if ($route = $collection->get('system.run_cron')) {
      $this->addPermissionToRoute($route, Settings::ACCESS_KEY_CRON);
    }
  }

  /**
   * Adds permissions to a given route.
   *
   * Enhances the route's requirements by adding the specified permission. The
   * conjunction key
   * (OR/AND) determines how the new permission is combined with existing
   * permissions.
   *
   * @param Route  $route
   *   The route to which the permission will be added.
   * @param string $permission
   *   The permission to be added.
   * @param string $conjunction
   *   The conjunction to use ('OR' or 'AND') for combining permissions.
   */
  protected function addPermissionToRoute(Route $route, string $permission, string $conjunction = 'OR'): void {
    $def_perm_str = $route->getRequirement('_permission');
    $con_key = $conjunction == 'OR' ? '+' : ',';
    $permission_str = $def_perm_str . $con_key . $permission;
    $route->setRequirement('_permission', $permission_str);
  }

}
