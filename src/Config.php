<?php

/**
 * @file
 * Contains the Config class for managing configuration settings in the Drupal
 * Codev-IT project.
 *
 * This file provides the Config class, which includes methods for overriding
 * configuration entities, managing view display modes, and setting user
 * redirect paths after login.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils;

use Drupal;
use Drupal\codev_utils\Helper\Field;
use Drupal\codev_utils\Helper\Utils;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Serialization\Yaml;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Drupal\views\Entity\View;
use Exception;

/**
 * Config utility class.
 *
 * Provides methods to manage configuration settings, including overriding
 * configurations, cloning and removing views display modes, and setting user
 * redirect paths after login.
 */
class Config {

  /**
   * Overrides configuration entities from a specified module.
   *
   * Allows overriding of configuration entities during install/update scripts.
   * The configuration files are sourced from the config/optional directory of
   * the specified module.
   *
   * @param string $config_name
   *   The name identifier of the config to override.
   * @param string $module
   *   The module from which to source the new configuration.
   * @param array  $excludes
   *   List of config keys to be excluded during override.
   */
  public static function override(string $config_name, string $module, array $excludes = ['langcode']): void {
    $module_path = Drupal::moduleHandler()->getModule($module)->getPath();
    $settings_file = sprintf('%s/config/optional/%s.yml', $module_path, $config_name);
    $settings = Yaml::decode(file_get_contents($settings_file));
    if (!empty($settings)) {
      foreach ($excludes as $prop) {
        unset($settings[$prop]);
      }
      $config = Drupal::configFactory()->getEditable($config_name);
      foreach ($settings as $key => $value) {
        $config->set($key, $value);
      }
      $config->save();
    }
  }

  /**
   * Overrides configuration translations from a specified module.
   *
   * Allows overriding of configuration translations during install/update
   * scripts. The configuration files are sourced from the
   * config/optional/language directory of the specified module.
   *
   * @param string $config_name
   *   The name identifier of the config to override.
   * @param string $module
   *   The module from which to source the new configuration.
   * @param array  $lang_codes
   *   Languages to override.
   * @param array  $excludes
   *   List of config keys to be excluded during override.
   *
   * @noinspection PhpUnused
   */
  public static function overrideTranslation(string $config_name, string $module, array $lang_codes = [], array $excludes = ['langcode']): void {
    $original_language = Drupal::languageManager()
      ->getConfigOverrideLanguage();

    foreach ($lang_codes as $lang_code) {
      $module_path = Drupal::moduleHandler()->getModule($module)->getPath();
      $settings_file = sprintf('%s/config/optional/%s/%s.yml',
        $module_path, $lang_code, $config_name);
      if (file_exists($settings_file)) {
        $settings = Yaml::decode(file_get_contents($settings_file));
        if (empty($settings)) {
          continue;
        }

        foreach ($excludes as $prop) {
          unset($settings[$prop]);
        }
        if ($original_language->getId() == $lang_code) {
          $config = Drupal::configFactory()->getEditable($config_name);
        }
        else {
          $config = Drupal::service('language.config_factory_override')
            ->getOverride($lang_code, $config_name);
        }
        foreach ($settings as $key => $value) {
          $config->set($key, $value);
        }
        $config->save();
      }
    }
  }

  /**
   * Clones a display mode in a view.
   *
   * Creates a clone of a specified display mode in a view with the option to
   * override settings.
   *
   * @param string $view_id
   *   View identifier.
   * @param string $view_mode
   *   View mode name.
   * @param array  $override
   *   Options to override settings in the cloned view mode.
   */
  public static function cloneViewsDisplayMode(string $view_id, string $view_mode, array $override): void {
    if (!empty($override['id'])) {
      $config = Drupal::configFactory()->getEditable('views.view.' . $view_id);
      $data = $config->getRawData();
      if (!$config->isNew()
        && !empty($data['display'][$view_mode])
        && empty($data['display'][$override['id']])) {
        $def_plugin = $data['display'][$view_mode]['display_plugin'];
        $display_plugin = Utils::getArrayValue('display_plugin', $override, $def_plugin);
        $defaults = $data['display']['default'];
        $new_display = $data['display'][$view_mode];
        $new_display['display_plugin'] = $display_plugin;
        if (!empty($override['display_options']['defaults'])
          && is_array($override['display_options']['defaults'])) {
          foreach ($override['display_options']['defaults'] as $option => $active) {
            if (!$active && !empty($defaults['display_options'][$option])) {
              $new_display['display_options'][$option] = $defaults['display_options'][$option];
            }
          }
          foreach ($override['display_options'] as $option => $value) {
            if ($option == 'fields'
              || $option == 'filters') {
              $elem = $override['display_options'][$option];
              foreach ($elem as $key => $active) {
                if (!$active && !empty($defaults['display_options'][$option])) {
                  unset($override['display_options'][$option][$key]);
                  unset($new_display['display_options'][$option][$key]);
                }
              }
            }
          }
        }
        $new_display = array_replace_recursive($new_display, $override);
        if (!empty($new_display)) {
          if ($new_display['display_plugin'] == 'block') {
            unset($new_display['display_options']['path'],
              $new_display['display_options']['menu'],
              $new_display['display_options']['tab_options']);
          }
          $data['display'][$new_display['id']] = $new_display;
        }
        $config->setData($data);
        $config->save();
      }
    }
  }

  /**
   * Removes a display mode from a view.
   *
   * Deletes a specified display mode from a view.
   *
   * @param string $view_id
   *   View identifier.
   * @param string $view_mode
   *   View mode name.
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   *
   * @noinspection PhpUnused
   */
  public static function removeViewsDisplayMode(string $view_id, string $view_mode): void {
    /** @var View $view */
    $view = Drupal::entityTypeManager()->getStorage('view')->load($view_id);
    if ($view instanceof View) {
      $displays = $view->get('display');
      if (!empty($displays[$view_mode])) {
        unset($displays[$view_mode]);
      }
      $view->set('display', $displays);
      $view->save();
    }
  }

  /**
   * Sets user redirect path after login.
   *
   * Configures a custom redirect path for a specified user role after login.
   *
   * @param string $role
   *   User role identifier.
   * @param string $url
   *   Redirect URL.
   *
   * @noinspection PhpUnused
   */
  public static function setUserRedirectAfterLogin(string $role, string $url): void {
    $role = Role::load($role);
    if (!empty($role) && $role instanceof RoleInterface) {
      $config = Drupal::configFactory()
        ->getEditable('redirect_after_login.settings');
      $login_redirection = $config->get('login_redirection') ?: [];
      $login_redirection[$role->id()] = $url;
      $config->set('login_redirection', $login_redirection);
      $config->save();
    }
  }

  /**
   * Adds meta tag field to an entity.
   *
   * Configures and adds a meta tag field to a specified entity bundle.
   *
   * @param string $entity_type
   *   Entity type identifier.
   * @param string $bundle
   *   Bundle name.
   *
   * @noinspection PhpUnused
   */
  public static function addMetaTag(string $entity_type, string $bundle): void {
    try {
      /** @noinspection SpellCheckingInspection */
      Field::addToEntity([
        'label'        => t('Meta-Tags'),
        'entity_type'  => $entity_type,
        'bundle'       => $bundle,
        'field_name'   => 'field_meta_tags',
        'field_type'   => 'metatag',
        'form_display' => [
          'type'     => 'metatag_firehose',
          'settings' => [
            'sidebar'     => TRUE,
            'use_details' => TRUE,
          ],
        ],
        'view_display' => [
          'type'  => 'metatag_empty_formatter',
          'label' => 'hidden',
        ],
      ]);
    } catch (Exception) {
    }
  }

}
