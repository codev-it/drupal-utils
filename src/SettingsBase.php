<?php

/**
 * @file
 * Contains the SettingsBase abstract class for managing settings in the Drupal
 * Codev-IT project.
 *
 * This file provides the SettingsBase class, which includes methods for
 * accessing and modifying configuration settings. This class serves as a base
 * for creating settings management classes for specific modules.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils;

use Drupal;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Exception;

/**
 * Abstract class for settings management.
 *
 * Provides a base structure for managing settings of modules, including methods
 * to read and write configuration values, and retrieve module paths.
 */
abstract class SettingsBase {

  /**
   * Name of the module associated with the settings.
   *
   * @var string
   */
  public const MODULE_NAME = '';

  /**
   * Name of the configuration settings.
   *
   * @var string
   */
  public const CONFIG_NAME = '';

  /**
   * Returns the module name.
   *
   * Retrieves the name of the module associated with the settings.
   *
   * @return string
   *   Module name.
   */
  protected static function getModuleName(): string {
    return static::MODULE_NAME ?: static::CONFIG_NAME;
  }

  /**
   * Returns the config name.
   *
   * Retrieves the name of the configuration settings.
   *
   * @return string
   *   Config name.
   */
  protected static function getConfigName(): string {
    return static::CONFIG_NAME ?: static::MODULE_NAME;
  }

  /**
   * Returns readonly settings.
   *
   * Retrieves the configuration settings as an immutable object.
   *
   * @return ImmutableConfig Immutable configuration object.
   */
  public static function getReadonly(): ImmutableConfig {
    $config = static::getConfigName() . '.settings';
    return Drupal::configFactory()->get($config);
  }

  /**
   * Returns editable settings.
   *
   * Retrieves the configuration settings as an editable object.
   *
   * @return Config Editable configuration object.
   */
  public static function getEditable(): Config {
    $config = static::getConfigName() . '.settings';
    return Drupal::configFactory()->getEditable($config);
  }

  /**
   * Converts settings to an array.
   *
   * Extracts the configuration settings as an associative array, excluding
   * specified keys.
   *
   * @param array $exclude
   *   List of keys to exclude.
   *
   * @return array
   *   Associative array of settings.
   */
  public static function toArray(array $exclude = []): array {
    $exclude[] = '_core';
    $arr = static::getReadonly()->getRawData();
    foreach ($exclude as $val) {
      if (!empty($arr[$val])) {
        unset($arr[$val]);
      }
    }
    return $arr;
  }

  /**
   * Sets a value in the settings by a given key.
   *
   * Saves a new value for a specified key in the configuration settings.
   *
   * @param string $key
   *   Setting key name.
   * @param mixed  $val
   *   New value to set.
   *
   * @return bool
   *   True if saved successfully, False otherwise.
   */
  public static function set(string $key, mixed $val): bool {
    try {
      static::getEditable()
        ->set($key, $val)
        ->save();
      return TRUE;
    } catch (Exception) {
      return FALSE;
    }
  }

  /**
   * Retrieves a value from the settings by a given key.
   *
   * Retrieves the value for a specified key from the configuration settings.
   *
   * @param string $key
   *   Setting key name.
   *
   * @return mixed
   *   The setting value or False if not found.
   */
  public static function get(string $key): mixed {
    return static::getReadonly()->get($key);
  }

  /**
   * Returns the current module path.
   *
   * Retrieves the file system path of the module associated with the settings.
   *
   * @param bool $absolute
   *   True for absolute path, False for relative path.
   *
   * @return string
   *   Module path.
   */
  public static function modulePath(bool $absolute = TRUE): string {
    $root = Drupal::root();
    $module_handler = Drupal::moduleHandler();
    $module_path = $module_handler
      ->getModule(static::getModuleName())
      ->getPath();
    return $absolute ? sprintf('%s/%s', $root, $module_path) : $module_path;
  }

}
