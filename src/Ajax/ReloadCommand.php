<?php

/**
 * @file
 * Defines the ReloadCommand class.
 *
 * Company: Codev-IT <office@codev-it.at>
 * Developer: Coser Angelo
 */

namespace Drupal\codev_utils\Ajax;

use Drupal\Core\Ajax\CommandInterface;

class ReloadCommand implements CommandInterface {

  /**
   * Indicates whether a throbber should be shown on page reload.
   *
   * @var string
   */
  private string $throbber;

  /**
   * The name of the element that triggered the command.
   *
   * @var string
   */
  private string $triggeringElement;

  /**
   * Constructs a ReloadCommand object.
   *
   * @param string $throbber
   *   An indicator for showing a throbber during the page reload.
   * @param string $triggering_element
   *   The name of the element that triggered the reload command.
   */
  public function __construct(string $throbber = '', string $triggering_element = '') {
    $this->throbber = $throbber;
    $this->triggeringElement = $triggering_element;
  }

  /**
   * Renders the custom AJAX command as an array suitable for AJAX responses.
   *
   * Returns an associative array that represents the custom 'reload' command.
   * The array includes parameters for showing a throbber and identifying the
   * triggering element during the page reload.
   *
   * @return array
   *   An associative array containing the 'reload' command and its associated
   *   parameters.
   *
   * @noinspection PhpUnused
   */
  public function render(): array {
    return [
      'command'           => 'reload',
      'throbber'          => $this->throbber,
      'triggeringElement' => $this->triggeringElement,
    ];
  }

}
