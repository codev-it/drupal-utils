<?php

/**
 * @file
 * Contains the definition of the HistoryUpdateCommand class.
 *
 * Company: Codev-IT <office@codev-it.at>
 * Developer: Coser Angelo
 */

namespace Drupal\codev_utils\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Provides a custom AJAX command to update the browser's history.
 *
 * This class implements the CommandInterface to create a custom AJAX command
 * named 'historyUpdate'. The command updates the browser's history stack by
 * adding a new entry with a specified URL, title, and additional data. It's
 * useful for scenarios where AJAX navigation should update the URL and title
 * in the browser's history without reloading the page.
 *
 * @noinspection PhpUnused
 */
class HistoryUpdateCommand implements CommandInterface {

  /**
   * The URL to be used for updating the history.
   *
   * @var Url|GeneratedUrl|string
   */
  private Url|GeneratedUrl|string $url;

  /**
   * The title to be set for the new history entry.
   *
   * @var string
   */
  private string $title;

  /**
   * Additional data to be associated with the history entry.
   *
   * @var array
   */
  private array $data;

  /**
   * Constructs a HistoryUpdateCommand object.
   *
   * @param string|Url                $url
   *   The URL as a string or Url object.
   * @param string|TranslatableMarkup $title
   *   The title for the history entry, can be a string or TranslatableMarkup.
   * @param array                     $data
   *   Additional data to be associated with the history entry.
   */
  public function __construct(string|Url $url, string|TranslatableMarkup $title = '', array $data = []) {
    $is_url = $url instanceof Url;
    $this->title = (string) $title;
    $this->url = $is_url ? $url->toString() : (string) $url;
    $this->data = $is_url ? array_merge($url->getRouteParameters(), $data) : $data;
  }

  /**
   * Renders the custom AJAX command as an array suitable for AJAX responses.
   *
   * Returns an associative array that represents the custom 'historyUpdate'
   * command. The array includes the URL, title, and additional data for the
   * history entry.
   *
   * @return array
   *   An associative array containing the 'historyUpdate' command and its
   *   associated parameters.
   *
   * @noinspection PhpUnused
   */
  public function render(): array {
    return [
      'command' => 'historyUpdate',
      'url'     => $this->url,
      'title'   => $this->title,
      'data'    => $this->data,
    ];
  }

}
