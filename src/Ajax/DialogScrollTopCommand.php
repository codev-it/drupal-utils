<?php

/**
 * @file
 * Provides an AJAX command to scroll a dialog to the top in a Drupal
 * environment.
 *
 * This file contains the DialogScrollTopCommand class, which implements the
 * CommandInterface to provide a custom AJAX command. The command is designed
 * to scroll the dialog window to the top, particularly useful in situations
 * where the dialog's content is dynamically updated, and it's important to
 * direct the user's attention to the beginning of the dialog.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Defines an AJAX command to scroll a dialog window to the top.
 *
 * This command is used in AJAX responses to ensure that the user is presented
 * with the top of the dialog, especially after dynamic content changes. It
 * implements the CommandInterface, providing a method to render the command as
 * an array suitable for AJAX responses.
 *
 * @noinspection PhpUnused
 */
class DialogScrollTopCommand implements CommandInterface {

  /**
   * Renders the command as an array suitable for AJAX responses.
   *
   * Returns an associative array representing the command. The 'command' key
   * holds the name of the command to be executed on the client side. This
   * command scrolls the dialog window to the top.
   *
   * @return array
   *   An associative array containing the 'command' key with the value
   *   'dialogScrollTop'.
   *
   * @noinspection PhpUnused
   */
  public function render(): array {
    return ['command' => 'dialogScrollTop'];
  }

}
