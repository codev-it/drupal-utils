<?php

/**
 * @file
 * Defines the ReplacePageTitleCommand class.
 *
 * Company: Codev-IT <office@codev-it.at>
 * Developer: Coser Angelo
 */

namespace Drupal\codev_utils\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a custom AJAX command for replacing the page title.
 *
 * The ReplacePageTitleCommand class defines a custom AJAX command that updates
 * the page title in the browser dynamically without requiring a full-page
 * reload. This class implements the CommandInterface and outputs an array to
 * specify the details of the title replacement command to the AJAX response
 * system.
 *
 * @noinspection PhpUnused
 */
class ReplacePageTitleCommand implements CommandInterface {

  /**
   * The new page title to be set.
   *
   * @var string
   */
  private string $title;

  /**
   * Constructs a ReplacePageTitleCommand object.
   *
   * @param string|Markup|TranslatableMarkup $title
   *   The new title for the page, which can be a string, Markup, or
   *   TranslatableMarkup object.
   */
  public function __construct(string|Markup|TranslatableMarkup $title) {
    $this->title = (string) $title;
  }

  /**
   * Renders the custom AJAX command as an array suitable for AJAX responses.
   *
   * Returns an associative array that represents the custom
   * 'replacePageTitleCommand'. The array includes the new title to be set for
   * the page.
   *
   * @return array
   *   An associative array containing the 'replacePageTitleCommand' and the
   *   new title.
   *
   * @noinspection PhpUnused
   */
  public function render(): array {
    return [
      'command' => 'replacePageTitleCommand',
      'title'   => $this->title,
    ];
  }

}
