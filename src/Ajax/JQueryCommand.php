<?php

/**
 * @file
 * Defines the JQueryCommand class for custom AJAX commands in the Drupal
 * Codev-IT project.
 *
 * This file provides the JQueryCommand class, which implements a custom AJAX
 * command that allows the execution of jQuery methods on selected elements. It
 * is used in AJAX responses to manipulate the DOM dynamically.
 *
 * Company: Codev-IT <office@codev-it.at>
 * Developer: Coser Angelo
 */

namespace Drupal\codev_utils\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Custom AJAX command for executing jQuery methods.
 *
 * This class implements the CommandInterface to provide a custom AJAX command
 * that can be used to apply jQuery methods to elements identified by a CSS
 * selector.
 *
 * @noinspection PhpUnused
 */
class JQueryCommand implements CommandInterface {

  /**
   * A CSS selector string to identify the elements to target.
   *
   * @var string
   */
  protected string $selector;

  /**
   * The name of the jQuery method to call on the selected elements.
   *
   * @var string
   */
  protected string $method;

  /**
   * Optional settings or arguments to be passed to the jQuery method.
   *
   * @var mixed
   */
  protected mixed $settings;

  /**
   * Constructs a JQueryCommand object.
   *
   * Initializes the command with the provided selector, jQuery method, and
   * optional settings.
   *
   * @param string $selector
   *   CSS selector to target specific elements.
   * @param string $method
   *   The jQuery method to execute on the selected elements.
   * @param mixed  $settings
   *   Optional settings or arguments for the jQuery method.
   */
  public function __construct(string $selector, string $method, mixed $settings = NULL) {
    $this->selector = $selector;
    $this->method = $method;
    $this->settings = $settings;
  }

  /**
   * Renders the custom AJAX command as an array suitable for AJAX responses.
   *
   * Returns an array representing the 'jQueryHandler' command with parameters
   * for the jQuery call, including the selector, method, and settings.
   *
   * @return array
   *   An associative array containing the 'jQueryHandler' command and its
   *   parameters.
   */
  public function render(): array {
    return [
      'command'  => 'jQueryHandler',
      'selector' => $this->selector,
      'method'   => $this->method,
      'settings' => $this->settings,
    ];
  }

}
