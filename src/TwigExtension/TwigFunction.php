<?php

/**
 * @file
 * Contains Twig extension functions for the Drupal Codev-IT project.
 *
 * This file provides the TwigFunction class, which defines custom Twig
 * functions used in the Drupal Codev-IT project. These functions include
 * utilities for rendering blocks, generating placeholder images, and
 * manipulating text content.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: TwigFunction.php
 */

namespace Drupal\codev_utils\TwigExtension;

use Drupal\codev_utils\Helper\Block;
use Drupal\codev_utils\Helper\Content;
use Drupal\codev_utils\Helper\Media;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction as TwigFunctionHandler;

/**
 * Custom Twig functions for the Drupal Codev-IT project.
 *
 * Provides a set of custom Twig functions that can be used in Twig templates.
 * These functions offer various utilities, such as rendering blocks,
 * generating placeholder images, and manipulating text content.
 *
 * @noinspection PhpUnused
 */
class TwigFunction extends AbstractExtension {

  /**
   * Returns the name of the Twig extension.
   *
   * @return string The name of the extension.
   */
  public function getName(): string {
    return 'codev_utils.twigFunction';
  }

  /**
   * Returns an array of custom Twig functions.
   *
   * Defines and returns an array of TwigFunctionHandler objects that represent
   * custom Twig functions. These functions provide various utilities for use
   * in Twig templates.
   *
   * @return array An array of TwigFunctionHandler objects.
   */
  public function getFunctions(): array {
    return [
      new TwigFunctionHandler('block_load',
        [Block::class, 'loadRenderable'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('block_build',
        [Block::class, 'buildRenderable'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('empty_image_markup',
        [Media::class, 'emptyImageMarkup'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('empty_image_markup_by_style',
        [Media::class, 'emptyImageMarkupByStyle'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('blind_text',
        [Content::class, 'getBlindText'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('cut_by_chars',
        [Content::class, 'subStrByChars'],
        ['is_safe' => ['html']]
      ),
      new TwigFunctionHandler('cut_by_words',
        [Content::class, 'subStrByWords'],
        ['is_safe' => ['html']]
      ),
    ];
  }

}
