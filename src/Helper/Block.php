<?php

/**
 * @file
 * Contains helper functions for handling blocks in Drupal.
 *
 * This file defines the Block class, which includes utility functions for
 * working with Drupal blocks. These functions provide capabilities to build
 * and load renderable arrays for blocks.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

use Drupal;
use Drupal\block\BlockInterface;
use Drupal\Core\Block\BlockBase;
use Exception;

/**
 * Helper toolbox class for handling blocks in Drupal.
 *
 * This class provides utility functions for working with Drupal blocks. It
 * includes methods to build and load renderable arrays for blocks,
 * facilitating the manipulation and rendering of block content.
 */
class Block {

  /**
   * Builds and returns a renderable array for a specified block ID.
   *
   * @param string $id
   *   The block ID.
   * @param array  $configuration
   *   An array of configuration settings for the block.
   *
   * @return array
   *   A renderable array representing the block.
   *
   * @noinspection PhpUnused
   */
  public static function buildRenderable(string $id, array $configuration = []): array {
    $ret = [];
    try {
      $block = Drupal::service('plugin.manager.block')
        ->createInstance($id, $configuration);
      if ($block instanceof BlockBase) {
        $ret = $block->build();
      }
    } catch (Exception) {
      return [];
    }
    return $ret;
  }

  /**
   * Loads and returns a renderable array for a specified block ID.
   *
   * @param string $id
   *   The block ID.
   *
   * @return array
   *   A renderable array representing the block.
   */
  public static function loadRenderable(string $id): array {
    $type_id = 'block';
    $entity_Type_Manager = Drupal::entityTypeManager();
    try {
      $block = $entity_Type_Manager
        ->getStorage($type_id)
        ->load($id);
      if ($block instanceof BlockInterface) {
        return $entity_Type_Manager
          ->getViewBuilder($type_id)
          ->view($block);
      }
    } catch (Exception) {
      return [];
    }
    return [];
  }

}
