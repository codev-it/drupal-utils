<?php

/**
 * @file
 * Contains helper functions for handling fields in Drupal.
 *
 * This file defines the Field class, which includes utility functions for
 * working with Drupal fields. These functions provide capabilities for field
 * data manipulation, field creation, and image style adjustments.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityDisplayRepository;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * Helper toolbox class for handling fields in Drupal.
 *
 * This class provides utility functions for working with Drupal fields. It
 * includes methods for field data extraction, field creation, and modifying
 * renderable image fields with different image styles.
 */
class Field {

  /**
   * Extracts clean field values from a fieldable entity.
   *
   * @param string                        $name
   *   The field name.
   * @param FieldableEntityInterface|null $entity
   *   The entity containing the field.
   * @param mixed|null                    $fallback
   *   Fallback value if the field is empty or not present.
   * @param bool                          $first
   *   Whether to return only the first value.
   * @param array                         $keys
   *   The array of field value keys to extract.
   *
   * @return mixed
   *   The extracted field values.
   */
  public static function fmtCleanValues(string $name, ?FieldableEntityInterface $entity, mixed $fallback = NULL, bool $first = TRUE, array $keys = ['value']): mixed {
    if (empty($name) || empty($keys)
      || !$entity instanceof FieldableEntityInterface
      || !$entity->hasField($name)) {
      return $fallback;
    }

    $field = $entity->get($name);
    if ($field->isEmpty()) {
      return $fallback;
    }

    $ret = [];
    $field_value = $field->getValue();
    foreach ($field_value as $id => $value) {
      foreach ($keys as $key) {
        if (!empty($value[$key])) {
          $ret[$id][$key] = $value[$key];
        }
        else {
          $ret[$id][$key] = $fallback;
        }
      }
      if (empty($ret[$id]) && !empty($value)) {
        $ret[$id] = $value;
      }
    }
    return $first && !empty($ret[0]) ? $ret[0] : $ret;
  }

  /**
   * Extracts a single clean field value from a fieldable entity.
   *
   * @param string                        $name
   *   The field name.
   * @param FieldableEntityInterface|null $entity
   *   The entity containing the field.
   * @param mixed|null                    $fallback
   *   Fallback value if the field is empty or not present.
   * @param bool                          $first
   *   Whether to return only the first value.
   * @param string                        $key
   *   The field value key to extract.
   *
   * @return mixed
   *   The extracted field value.
   */
  public static function fmtCleanValue(string $name, ?FieldableEntityInterface $entity, mixed $fallback = NULL, bool $first = TRUE, string $key = 'value'): mixed {
    if ($entity === NULL || empty($key)) {
      return $fallback;
    }

    $values = static::fmtCleanValues($name, $entity, $fallback, $first, [$key]);
    if ($values == $fallback) {
      return $values;
    }

    if ($first) {
      return $values[$key] ?? $fallback;
    }

    $ret = [];
    foreach ($values as $value) {
      if (!empty($value[$key])) {
        $ret[] = $value[$key];
      }
    }
    return $ret ?: $fallback;
  }

  /**
   * Modifies renderable image fields to use a specific image style.
   *
   * @param array|null $field
   *   The renderable image field array.
   * @param string     $image_style
   *   The image style to apply.
   */
  public static function changeImageStyle(?array &$field, string $image_style): void {
    if (!empty($field)) {
      $items = Element::children($field);
      if (!empty($items)) {
        foreach ($items as $i) {
          $field[$i]['#image_style'] = $image_style;
        }
      }
    }
  }

  /**
   * Adds a new field to an entity type and bundle.
   *
   * @param array $values
   *   The values for creating the field.
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   * @throws MissingDataException
   */
  public static function addToEntity(array $values = []): void {
    $depend_props = [
      'label',
      'entity_type',
      'bundle',
      'field_name',
      'field_type',
    ];
    foreach ($depend_props as $prop) {
      if (empty($values[$prop])) {
        throw new MissingDataException(sprintf('required property "%s" is missing.', $prop));
      }
    }

    $entity_type = $values['entity_type'];
    $bundle = $values['bundle'];
    $field_name = $values['field_name'];
    $entity_manager = Drupal::entityTypeManager();
    $field_config = $entity_manager->getStorage('field_config');
    if (!$field_config->load($entity_type . '.' . $bundle . '.' . $field_name)) {
      $field_storage_config = $entity_manager->getStorage('field_storage_config');
      if (!$field_storage_config->load($entity_type . '.' . $field_name)) {
        $field_storage_config->create([
          'entity_type' => $entity_type,
          'field_name'  => $field_name,
          'type'        => $values['field_type'],
          'settings'    => $values['storage_settings'] ?? [],
          'cardinality' => $values['cardinality'] ?? 1,
        ])->save();
      }
      $field_config->create([
        'label'       => $values['label'] ?? '',
        'entity_type' => $entity_type,
        'bundle'      => $bundle,
        'field_name'  => $field_name,
        'settings'    => $values['settings'] ?? [],
      ])->save();

      /** @var EntityDisplayRepository $entity_display */
      $entity_display = Drupal::service('entity_display.repository');
      $form_display_settings = $values['form_display'] ?? NULL;
      $view_display_settings = $values['view_display'] ?? NULL;
      if ($form_display_settings === TRUE || is_array($form_display_settings)) {
        $form_display = $entity_display->getFormDisplay($entity_type, $bundle);
        $form_display->setComponent($field_name,
          is_array($form_display_settings) ? $form_display_settings : []);
        $form_display->save();
      }
      if ($view_display_settings === TRUE || is_array($view_display_settings)) {
        $view_display = $entity_display->getViewDisplay($entity_type, $bundle);
        $view_display->setComponent($field_name,
          is_array($view_display_settings) ? $view_display_settings : []);
        $view_display->save();
      }
    }
  }

}
