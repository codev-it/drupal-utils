<?php

/** @noinspection PhpUndefinedNamespaceInspection */
/** @noinspection PhpUndefinedClassInspection */

/**
 * @file
 * Defines the Profile helper class for managing user profiles in the Drupal
 * Codev-IT project.
 *
 * This file provides the Profile class, which includes methods for loading and
 * managing user profiles. These methods are used to interact with the profile
 * entity in Drupal.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

use Drupal;
use Drupal\Core\Session\AccountInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\profile\ProfileStorageInterface;
use Exception;

/**
 * Profile helper class.
 *
 * Provides methods for loading and retrieving data from user profiles. These
 * methods interact with the profile entity in Drupal, offering functionality to
 * access profile information.
 *
 * @noinspection PhpUnused
 */
class Profile {

  /**
   * Loads the default profile for a user.
   *
   * Retrieves the default profile of a specified user for a given profile
   * type.
   * If no user is provided, the current user's default profile is returned.
   *
   * @param AccountInterface|null $account User account to load the profile
   *                                       for, or NULL for the current user.
   * @param string                $type    Profile type to load.
   *
   * @return ProfileInterface|null The user's default profile or NULL if not
   *                               found.
   */
  public static function loadDefault(AccountInterface $account = NULL, string $type = 'customer'): ?ProfileInterface {
    try {
      if (empty($account)) {
        $account = Drupal::currentUser()->getAccount();
      }
      /** @var ProfileStorageInterface $profile_storage */
      $profile_storage = Drupal::entityTypeManager()->getStorage('profile');
      return $profile_storage->loadByUser($account, $type);
    } catch (Exception) {
      return NULL;
    }
  }

  /**
   * Loads all profiles for a user.
   *
   * Retrieves all profiles associated with a specified user for a given
   * profile type. If no user is provided, all profiles for the current user
   * are returned.
   *
   * @param AccountInterface|null $account User account to load the profiles
   *                                       for, or NULL for the current user.
   * @param string                $type    Profile type to load.
   *
   * @return ProfileInterface[] An array of user profiles.
   *
   * @noinspection PhpUnused
   */
  public static function loadAll(AccountInterface $account = NULL, string $type = 'customer'): array {
    try {
      if (empty($account)) {
        $account = Drupal::currentUser()->getAccount();
      }
      /** @var ProfileStorageInterface $profile_storage */
      $profile_storage = Drupal::entityTypeManager()->getStorage('profile');
      return $profile_storage->loadMultipleByUser($account, $type);
    } catch (Exception) {
      return [];
    }
  }

  /**
   * Retrieves address information from a profile.
   *
   * Extracts and returns address information from a given profile entity.
   *
   * @param ProfileInterface|null $profile Profile entity to retrieve the
   *                                       address from.
   *
   * @return array Address information as an associative array, or an empty
   *               array if not available.
   */
  public static function getAddress(?ProfileInterface $profile): array {
    if (empty($profile)) {
      return [];
    }
    try {
      return !$profile->get('address')->isEmpty()
        ? $profile->get('address')->first()->getValue() : [];
    } catch (Exception) {
      return [];
    }
  }

}
