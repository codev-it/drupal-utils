<?php

/**
 * @file
 * Contains helper functions for handling content and nodes in Drupal.
 *
 * This file defines the Content class, which includes utility functions for
 * working with Drupal content and nodes. These functions provide capabilities
 * for generating blind text, substring manipulation, and more.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

/**
 * Helper toolbox class for handling content and nodes in Drupal.
 *
 * This class provides utility functions for working with Drupal content and
 * nodes. It offers methods for generating blind text and substring
 * manipulation, aiding in content presentation and formatting.
 */
class Content {

  /**
   * Default blind text string.
   *
   * @var string
   */
  public const NODE_BLIND_TEXT = 'Lorem ipsum...';

  /**
   * Generates blind text with optional paragraph wrapping.
   *
   * @param bool $wrapInParagraphTag
   *   Whether to wrap the text in paragraph tags.
   *
   * @return string
   *   The generated blind text.
   */
  public static function getBlindText(bool $wrapInParagraphTag = TRUE): string {
    $output = self::NODE_BLIND_TEXT;
    if ($wrapInParagraphTag) {
      $output = '<p>' . $output . '</p>';
    }
    return $output;
  }

  /**
   * Truncates a string by characters, optionally cutting only by new words.
   *
   * @param string $text
   *   The text to be truncated.
   * @param int    $max_char
   *   Maximum characters to include in the output.
   * @param string $end
   *   Text to append at the end of the truncated string.
   * @param bool   $cut_only_by_new_word
   *   Whether to truncate only at word boundaries.
   *
   * @return string
   *   The truncated string.
   */
  public static function subStrByChars(string $text, int $max_char, string $end = '...', bool $cut_only_by_new_word = TRUE): string {
    if (strlen($text) < $max_char) {
      return $text;
    }

    $output = substr($text, 0, $max_char);
    if ($cut_only_by_new_word) {
      $output = '';
      $length = 0;
      $words = preg_split('/\s/', $text);
      foreach ($words as $word) {
        if ($length < $max_char) {
          $output .= ' ' . $word;
          $length = $length + strlen($word) + 1;
        }
      }
      $output = trim($output);
    }
    return $output . $end;
  }

  /**
   * Truncates a string by words.
   *
   * @param string $text
   *   The text to be truncated.
   * @param int    $max_char
   *   Maximum words to include in the output.
   * @param string $end
   *   Text to append at the end of the truncated string.
   *
   * @return string
   *   The truncated string.
   */
  public static function subStrByWords(string $text, int $max_char, string $end = '...'): string {
    $words = preg_split('/\s/', $text) ?: [];
    if (count($words) < $max_char) {
      return $text;
    }
    $output = implode(' ', array_splice($words, 0, $max_char));
    return $output . $end;
  }

}
