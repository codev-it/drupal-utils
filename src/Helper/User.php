<?php

/**
 * @file
 * Contains helper functions for user-related operations in Drupal.
 *
 * This file defines the User class within the Codev-IT Drupal module. The User
 * class includes a collection of helper functions designed to assist with
 * various user-related tasks. These functions enable developers to work
 * more efficiently with user entities and roles within Drupal.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

use Drupal;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\user\Entity\Role;
use Exception;

/**
 * User helper class for Drupal.
 *
 * The User class offers a suite of utility functions for working with user
 * entities and roles in Drupal. It simplifies tasks such as retrieving user
 * roles, sorting roles by weight, and managing user permissions. This class
 * enhances the ease of user management in Drupal projects.
 */
class User {

  /**
   * Get an array of roles under the current user's role.
   *
   * Diese Methode liefert eine Liste von Benutzerrollen, die unterhalb der
   * Rolle des aktuellen Benutzers liegen. Dies ist nützlich, um die Rollen
   * zu bestimmen, die einem Benutzer zugewiesen werden können, basierend auf
   * seiner aktuellen Rolle und Berechtigung.
   *
   * @param bool              $highest
   *   Wenn auf 'true' gesetzt, wird die höchste autorisierte Benutzergruppe
   *   ebenfalls eingeschlossen. Wenn 'false', werden nur Rollen unterhalb
   *   der aktuellen Benutzerrolle zurückgegeben.
   * @param AccountProxy|null $account
   *   Benutzerkonto-Objekt oder 'null'. Wenn 'null', wird der aktuelle
   *   Benutzer automatisch bestimmt.
   * @param bool              $sorted
   *   Wenn auf 'true' gesetzt, werden die Rollen nach Gewicht sortiert.
   *
   * @return array
   *   Ein Array von Rollenobjekten.
   */
  public static function getAllRoles(bool $highest = FALSE, bool $sorted = FALSE, ?AccountInterface $account = NULL): array {
    if (!$account) {
      $account = Drupal::currentUser();
    }
    try {
      if (!$account->isAnonymous()) {
        $roles = Drupal::entityTypeManager()
          ->getStorage('user_role')
          ->loadMultiple();
        $max_weight = NULL;
        foreach ($account->getRoles() as $role_id) {
          /** @var Role $role */
          if ($role = $roles[$role_id] ?? NULL) {
            if ($role->getWeight() > $max_weight) {
              $max_weight = $role->getWeight();
            }
          }
        }

        /** @var Role $role */
        foreach ($roles as $role) {
          if ($highest) {
            if ($role->getWeight() > $max_weight) {
              unset($roles[$role->id()]);
            }
          }
          else {
            if ($role->getWeight() >= $max_weight) {
              unset($roles[$role->id()]);
            }
          }
        }
        if ($sorted) {
          Utils::sortObjectByMethod($roles);
        }
        return $roles;
      }
      return [];
    } catch (Exception) {
      return [];
    }
  }

}
