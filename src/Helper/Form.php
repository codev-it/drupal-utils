<?php

/**
 * @file
 * Contains helper functions for handling forms in Drupal.
 *
 * This file defines the Form class, which includes utility functions for
 * working with Drupal forms. These functions provide capabilities for form
 * element manipulation and form validation.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

/**
 * Helper toolbox class for handling forms in Drupal.
 *
 * This class provides utility functions for working with Drupal forms. It
 * includes methods for form element name construction and form validation
 * checks.
 */
class Form {

  /**
   * Builds a form item name string from its parent array.
   *
   * @param array $parent
   *   An array of parent elements for the form item.
   *
   * @return string
   *   The constructed form item name.
   */
  public static function buildFormItemName(array $parent = []): string {
    if (empty($parent)) {
      return '';
    }

    if (count($parent) === 1) {
      return $parent[0];
    }

    $prefix = $parent[0];
    unset($parent[0]);
    return sprintf('%s[%s]', $prefix, implode('][', $parent));
  }

}
