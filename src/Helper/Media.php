<?php

/**
 * @file
 * Provides utility functions for handling media content in Drupal.
 *
 * This file defines the Media class within the Codev-IT Drupal module. The
 * Media class includes a collection of helper functions designed to assist
 * with various tasks related to media entities and files. These functions
 * enable developers to generate URLs for media and image files, create
 * renderable arrays for displaying images with specific styles, and manage
 * image attributes and styles within Drupal.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Media.php
 */

namespace Drupal\codev_utils\Helper;

use Drupal;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\Entity\Media as MediaEntity;
use Exception;

/**
 * Media helper class for Drupal.
 *
 * The Media class offers a suite of utility functions for working with media
 * content in Drupal. It simplifies tasks such as retrieving URLs for image
 * files, generating renderable arrays for images with specified styles,
 * and encoding files in Base64 format. This class enhances the ease of
 * media management in Drupal projects.
 */
class Media {

  /**
   * Gets the URL of an image file.
   *
   * @param File   $file
   *   The file entity.
   * @param string $image_style
   *   The image style identifier.
   * @param bool   $relative
   *   Whether to return a relative URL.
   *
   * @return string
   *   The image URL or an empty string if the file is not found.
   */
  public static function getUrlFromFile(File $file, string $image_style = '', bool $relative = TRUE): string {
    /** @var FileUrlGenerator $file_url_generator */
    $file_url_generator = Drupal::service('file_url_generator');
    $image_uri = $file->getFileUri();
    if (!empty($image_style) && $style = ImageStyle::load($image_style)) {
      if (!file_exists($style->buildUri($image_uri))) {
        $style->createDerivative($image_uri, $style->buildUri($image_uri));
      }
      $image_uri = $style->buildUri($image_uri);
    }
    return $relative ? $file_url_generator->generateString($image_uri)
      : $file_url_generator->generateAbsoluteString($image_uri);
  }

  /**
   * Gets the URL of an image file by its file entity ID.
   *
   * @param mixed  $fid
   *   The file entity ID.
   * @param string $image_style
   *   The image style identifier.
   * @param bool   $relative
   *   Whether to return a relative URL.
   *
   * @return string
   *   The image URL or an empty string if the file is not found.
   */
  public static function getUrlByFileId(mixed $fid, string $image_style = '', bool $relative = TRUE): string {
    if (empty($fid)) {
      return '';
    }

    try {
      $file = Drupal::entityTypeManager()
        ->getStorage('file')
        ->load($fid);
      if ($file instanceof File) {
        return static::getUrlFromFile($file, $image_style, $relative);
      }
    } catch (Exception) {
      return '';
    }
    return '';
  }

  /**
   *  Gets the URL of a media image file.
   *
   * @param MediaEntity $media
   *   The media entity ID.
   * @param string      $image_style
   *   The image style identifier.
   * @param bool        $relative
   *   Whether to return a relative URL.
   * @param string      $field_name
   *   The field name of the media image.
   *
   * @return string
   *   The image URL or an empty string if the media entity or image is not
   *   found.
   *
   * @noinspection PhpUnused
   */
  public static function getUrlByMedia(MediaEntity $media, string $image_style = '', bool $relative = TRUE, string $field_name = 'field_media_image'): string {
    if ($image_id = Field::fmtCleanValue($field_name, $media, NULL, TRUE, 'target_id')) {
      return static::getUrlByFileId($image_id, $image_style, $relative);
    }
    return '';
  }

  /**
   * Gets the URL of an image associated with a media entity by its ID.
   *
   * @param mixed  $mid
   *   The media entity ID.
   * @param string $image_style
   *   The image style identifier.
   * @param bool   $relative
   *   Whether to return a relative URL.
   * @param string $field_name
   *   The field name of the media image.
   *
   * @return string
   *   The image URL or an empty string if the media entity or image is not
   *   found.
   *
   * @noinspection PhpUnused
   */
  public static function getUrlByMediaId(mixed $mid, string $image_style = '', bool $relative = TRUE, string $field_name = 'field_media_image'): string {
    if (empty($mid)) {
      return '';
    }

    try {
      $media = Drupal::entityTypeManager()
        ->getStorage('media')
        ->load($mid);
      if ($media instanceof MediaEntity
        && $image_id = Field::fmtCleanValue($field_name, $media, NULL, TRUE, 'target_id')) {
        return static::getUrlByFileId($image_id, $image_style, $relative);
      }
    } catch (Exception) {
      return '';
    }
    return '';
  }

  /**
   * Image style theme renderable array wrapper function.
   *
   * @param string $uri
   * @param string $style
   * @param array  $opts
   *
   * @return array
   */
  public static function themeImageStyleMarkup(string $uri, string $style = 'thumbnail', array $opts = []): array {
    $ret = [];
    foreach ($opts as $key => $val) {
      if (!empty($val)) {
        $ret['#' . $key] = $val;
      }
    }
    $ret['#theme'] = 'image_style';
    $ret['#uri'] = $uri;
    $ret['#style_name'] = $style;
    return $ret;
  }

  /**
   * Image style theme renderable array wrapper function.
   *
   * @param File|null $file
   * @param string    $style
   * @param array     $opts
   *
   * @return array
   *
   * @noinspection PhpUnused
   */
  public static function renderableImage(?File $file, string $style = 'thumbnail', array $opts = []): array {
    return !empty($file) ? static::themeImageStyleMarkup($file->getFileUri(), $style, $opts) : [];
  }

  /**
   * Image style theme renderable array wrapper function.
   *
   * @param File|null $file
   *
   * @return string
   */
  public static function buildBase64(?File $file): string {
    if (!$file) {
      return '';
    }

    $file_path = Drupal::service('file_system')
      ->realpath($file->getFileUri());
    if (file_exists($file_path)) {
      $base64_data = base64_encode(file_get_contents($file_path));
      return sprintf('data:%s;base64,%s', $file->getMimeType(), $base64_data);
    }
    return '';
  }

  /**
   * Return empty image markup.
   *
   * @param int    $width
   * @param int    $height
   * @param string $alt
   * @param string $title
   *
   * @return array
   */
  public static function emptyImageMarkup(int $width, int $height, string $alt = '', string $title = ''): array {
    $im = imagecreatetruecolor($width, $height);
    $black = imagecolorallocate($im, 0, 0, 0);
    imagecolortransparent($im, $black);
    ob_start();
    imagegif($im);
    $contents = ob_get_contents();
    ob_end_clean();
    imagedestroy($im);
    return [
      '#theme'  => 'image',
      '#uri'    => 'data:image/gif;base64,' . base64_encode($contents),
      '#width'  => $width,
      '#height' => $height,
      '#alt'    => $alt,
      '#title'  => $title,
    ];
  }

  /**
   * Return a empty image markup, bei image style.
   *
   * @param string $image_style
   * @param string $alt
   * @param string $title
   *
   * @return array
   */
  public static function emptyImageMarkupByStyle(string $image_style, string $alt = '', string $title = ''): array {
    $width = 1;
    $height = 1;
    /** @var ImageStyle $style */
    $style = ImageStyle::load($image_style);
    $accept_effect = ['image_scale', 'image_scale_and_crop'];

    if ($style->getEffects()->count()) {
      try {
        /** @var Drupal\image\ConfigurableImageEffectInterface $item */
        foreach ($style->getEffects()->getIterator() as $item) {
          if (in_array($item->getPluginId(), $accept_effect)) {
            $effect = $item->getConfiguration();
            if ($width === 1 && !empty($effect['data']['width'])) {
              $width = $effect['data']['width'];
            }
            if ($height === 1 && !empty($effect['data']['height'])) {
              $height = $effect['data']['height'];
            }
          }
        }
      } catch (Exception) {
      }
    }
    return static::emptyImageMarkup($width, $height, $alt, $title);
  }

}
