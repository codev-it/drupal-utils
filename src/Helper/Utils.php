<?php

/**
 * @file
 * Contains the Utils class with utility methods for Drupal Codev-IT projects.
 *
 * This file includes the Utils class, which provides a collection of utility
 * methods used across Drupal Codev-IT projects. These methods include array
 * and object manipulation, string processing, and encryption utilities.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils\Helper;

use Drupal\codev_utils\Settings;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Site\Settings as SiteSettings;

/**
 * Utility toolbox class, including useful functions.
 *
 * This class provides a collection of static utility functions that can be
 * used throughout Drupal projects. It includes methods for array manipulation,
 * dialog attributes, encryption, and more.
 */
class Utils {

  /**
   * Merge attributes arrays.
   *
   * Merges multiple arrays of attributes into a single array. If multiple
   * arrays contain the same keys, their values are combined into an array. For
   * the 'id' attribute, if multiple values are present, the last value is
   * used.
   *
   * @param array ...$arrays
   *   Variable number of attribute arrays to merge.
   *
   * @return array
   *   Merged array of attributes.
   */
  public static function mergeAttributesArray(array ...$arrays): array {
    $merged = array_merge_recursive(...$arrays);
    if (!empty($merged['id']) && is_array($merged['id']) && count($merged['id']) >= 2) {
      $merged['id'] = end($merged['id']);
    }
    foreach ($merged as &$item) {
      if (is_array($item)) {
        $item = array_unique($item);
      }
    }
    return $merged;
  }

  /**
   * Append modal dialog options attributes to given array.
   *
   * @param array $attr
   *   Array of attributes.
   * @param array $opts
   *   Array of modal dialog options.
   */
  public static function appendDialogModalAttr(array &$attr = [], array $opts = []): void {
    $opts = array_merge(['width' => Settings::get('dialog_width')], $opts);
    $attr['class'][] = 'use-ajax';
    $attr['data-dialog-type'] = 'modal';
    $attr['data-dialog-options'] = Json::encode($opts);
  }

  /**
   * Return the value from the given array, or the fallback if empty.
   *
   * @param string $key
   *   The key to look for in the array.
   * @param array  $arr
   *   The array to search.
   * @param mixed  $fallback
   *   The value to return if the key is empty in the array.
   *
   * @return mixed
   *   The value from the array or the fallback value.
   */
  public static function getArrayValue(string $key, array $arr = [], mixed $fallback = NULL): mixed {
    return !empty($arr[$key]) ? $arr[$key] : $fallback;
  }

  /**
   * Return the value from the given array nested, or the fallback if empty.
   *
   * This is an extended function with fallback for the Drupal
   * NestedArray::getValue() utility.
   *
   * @param string $keys
   *   Dot-separated keys to access nested values.
   * @param array  $arr
   *   The array to search.
   * @param mixed  $fallback
   *   The value to return if the nested key is empty in the array.
   *
   * @return mixed
   *   The nested value from the array or the fallback value.
   */
  public static function getArrayValueNested(string $keys, array $arr = [], mixed $fallback = NULL): mixed {
    return NestedArray::getValue($arr, explode('.', $keys)) ?: $fallback;
  }

  /**
   * Check if a PHP array is associative or sequential.
   *
   * @param array $arr
   *   The array to check.
   *
   * @return bool
   *   TRUE if the array is associative, FALSE if it is sequential.
   */
  public static function isArrayAssoc(array $arr): bool {
    if (empty($arr)) {
      return FALSE;
    }
    return array_keys($arr) !== range(0, count($arr) - 1);
  }

  /**
   * Sort array by key.
   *
   * @param array  $arr
   *   The array to sort.
   * @param string $key
   *   The key to use for sorting.
   * @param string $sort
   *   Sort order ('asc' or 'desc').
   * @param bool   $keep_index
   *   Whether to keep the original index of the array elements.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function sortArrayByKey(array &$arr, string $key = 'weight', string $sort = 'desc', bool $keep_index = TRUE): bool {
    $sort_fn = function (array $a, array $b) use ($key, $sort) {
      $val_a = $a[$key];
      $val_b = $b[$key];
      if ($val_a == $val_b) {
        return 0;
      }
      if ($sort === 'desc') {
        return $val_a < $val_b ? -1 : 1;
      }
      else {
        return $val_a > $val_b ? -1 : 1;
      }
    };
    if ($keep_index) {
      return uasort($arr, $sort_fn);
    }
    else {
      return usort($arr, $sort_fn);
    }
  }

  /**
   * Sort objects by property name.
   *
   * @param array  $arr
   *   The array of objects to sort.
   * @param string $prop
   *   The property name to use for sorting.
   * @param string $sort
   *   Sort order ('asc' or 'desc').
   * @param bool   $keep_index
   *   Whether to keep the original index of the array elements.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function sortObjectByProp(array &$arr, string $prop = 'weight', string $sort = 'desc', bool $keep_index = TRUE): bool {
    $sort_fn = function (object $a, object $b) use ($prop, $sort) {
      $val_a = $a->{$prop};
      $val_b = $b->{$prop};
      if ($val_a == $val_b) {
        return 0;
      }
      if ($sort === 'desc') {
        return $val_a < $val_b ? -1 : 1;
      }
      else {
        return $val_a > $val_b ? -1 : 1;
      }
    };
    if ($keep_index) {
      return uasort($arr, $sort_fn);
    }
    else {
      return usort($arr, $sort_fn);
    }
  }

  /**
   * Sort objects by method result.
   *
   * @param array  $arr
   *   The array of objects to sort.
   * @param string $method
   *   The method name to use for sorting.
   * @param string $sort
   *   Sort order ('asc' or 'desc').
   * @param bool   $keep_index
   *   Whether to keep the original index of the array elements.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public static function sortObjectByMethod(array &$arr, string $method = 'getWeight', string $sort = 'desc', bool $keep_index = TRUE): bool {
    $sort_fn = function (object $a, object $b) use ($method, $sort) {
      $val_a = $a->{$method}();
      $val_b = $b->{$method}();
      if ($val_a == $val_b) {
        return 0;
      }
      if ($sort === 'desc') {
        return $val_a < $val_b ? -1 : 1;
      }
      else {
        return $val_a > $val_b ? -1 : 1;
      }
    };
    if ($keep_index) {
      return uasort($arr, $sort_fn);
    }
    else {
      return usort($arr, $sort_fn);
    }
  }

  /**
   * Split a string separated by commas or whitespace into an array.
   *
   * @param string|null $str
   *   The string to split.
   *
   * @return array
   *   An array containing the split values.
   */
  public static function strSplitValues(?string $str): array {
    if (!$str) {
      return [];
    }
    $formatted_str = str_replace(' ', ',', $str);
    $arr = array_unique(explode(',', $formatted_str));
    return array_values(array_filter($arr, function ($val) {
      return !empty(trim($val));
    }) ?: []);
  }

  /**
   * Load the translation from the given PO file.
   *
   * @param string $translation
   *   The translation to look for.
   * @param string $file
   *   The path to the PO file.
   *
   * @return string
   *   The translated string, or the original translation if not found.
   */
  public static function poT(string $translation, string $file): string {
    if (empty($file) || !file_exists($file)) {
      return $translation;
    }

    $msgid = NULL;
    $fileContents = file_get_contents($file);
    $lines = explode("\n", $fileContents);

    foreach ($lines as $line) {
      if (preg_match('/^msgid "(.*)"$/', $line, $matches)) {
        $msgid = $matches[1];
      }
      elseif (preg_match('/^msgstr "(.*)"$/', $line, $matches) && $translation == $msgid) {
        return $matches[1];
      }
    }
    return $translation;
  }

  /**
   * Crypt default data.
   *
   * @return array
   *   An array containing cryptographic data.
   *
   * @noinspection PhpUnused
   */
  protected static function crypt(): array {
    $secret_key = SiteSettings::getHashSalt();
    $secret_iv = '';
    for ($i = 0; $i <= 4; $i++) {
      $offset = $i + 2;
      $secret_iv .= substr($secret_key, $offset, $offset + 8);
    }
    return [
      'encrypt_method' => 'AES-256-CBC',
      'key'            => hash('sha256', $secret_key),
      'iv'             => substr(hash('sha256', $secret_iv), 0, 16),
    ];
  }

  /**
   * Encrypt the given string.
   *
   * @param string $str
   *   The string to encrypt.
   *
   * @return string|null
   *   The encrypted string, or NULL on failure.
   *
   * @noinspection PhpUnused
   */
  public static function encrypt(string $str): ?string {
    $crypt = static::crypt();
    $output = openssl_encrypt($str, $crypt['encrypt_method'], $crypt['key'], 0, $crypt['iv']);
    return base64_encode($output ?: '') ?: NULL;
  }

  /**
   * Decrypt the given hash.
   *
   * @param string $hash
   *   The hash to decrypt.
   *
   * @return string|null
   *   The decrypted string, or NULL on failure.
   *
   * @noinspection PhpUnused
   */
  public static function decrypt(string $hash): ?string {
    $crypt = static::crypt();
    return openssl_decrypt(base64_decode($hash), $crypt['encrypt_method'], $crypt['key'], 0, $crypt['iv']) ?: NULL;
  }

}
