<?php

/**
 * @file
 * Contains the Settings class for managing configuration settings in the
 * Drupal Codev-IT project.
 *
 * This file provides the Settings class, which extends SettingsBase and
 * includes constants representing various configuration settings and
 * permission keys specific to the Codev-IT project.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\codev_utils;

/**
 * Settings class for the Codev-IT project.
 *
 * Defines constants for various configuration settings and permission keys
 * used
 * throughout the Codev-IT project. These constants provide a standardized way
 * to reference common settings and permissions.
 */
class Settings extends SettingsBase {

  /**
   * Module name for the Codev-IT utilities.
   *
   * @var string
   */
  public const MODULE_NAME = 'codev_utils';

  /**
   * Permission key for administering permissions.
   *
   * @var string
   */
  public const ACCESS_KEY_PERMISSION = 'administer permissions';

  /**
   * Permission admin access key.
   *
   * @var string
   */
  public const ACCESS_KEY_PERMISSION_ADMIN = 'administer permissions admin';

  /**
   * Permission view status report page.
   *
   * @var string
   */
  public const ACCESS_KEY_VIEW_STATUS_REPORT = 'view status report';

  /**
   * Permission administer basic site settings.
   *
   * @var string
   */
  public const ACCESS_KEY_BASIC_SETTINGS = 'administer basic site settings';

  /**
   * Permission administer basic site settings.
   *
   * @var string
   */
  public const ACCESS_KEY_CRON = 'access cron';

  /**
   * Permission administer basic site settings.
   *
   * @var string
   */
  public const ACCESS_KEY_CACHE = 'access cache';

  /**
   * Permission administer basic site settings.
   *
   * @var string
   */
  public const ACCESS_KEY_BLOCKS = 'administer blocks';

  /**
   * Permission administer basic site settings.
   *
   * @var string
   */
  public const ACCESS_KEY_THEME = 'administer themes %s';

  /**
   * Route name for the user login page.
   *
   * @var string
   */
  public const USER_LOGIN_ROUTE = 'user.login';

  /**
   * Route name for the user registration page.
   *
   * @var string
   */
  public const USER_REGISTER_ROUTE = 'user.register';

  /**
   * Route name for the user password reset page.
   *
   * @var string
   */
  public const USER_PASS_RESET_ROUTE = 'user.pass';

}
