<?php

namespace Drupal\codev_utils\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AccountMenuModal.php
 * .
 */

/**
 * Class AccountMenuModal.
 *
 * Provides block plugin definitions for custom menus.
 *
 * @see          \Drupal\codev_utils\Plugin\Block\AccountMenuModal
 *
 * @noinspection PhpUnused
 */
class AccountMenuModal extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The menu storage.
   *
   * @var EntityStorageInterface
   */
  protected EntityStorageInterface $menuStorage;

  /**
   * Constructs new SystemMenuBlock.
   *
   * @param EntityStorageInterface $menu_storage
   *   The menu storage.
   */
  public function __construct(EntityStorageInterface $menu_storage) {
    $this->menuStorage = $menu_storage;
  }

  /**
   * {@inheritDoc}
   *
   * @return AccountMenuModal|ContainerDeriverInterface
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public static function create(ContainerInterface $container, $base_plugin_id): AccountMenuModal|ContainerDeriverInterface|static {
    return new static($container->get('entity_type.manager')
      ->getStorage('menu'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $menu = $this->menuStorage->load('account');
    if (!empty($menu)) {
      $admin_label = $base_plugin_definition['admin_label'];
      $this->derivatives[$menu->id()] = $base_plugin_definition;
      $this->derivatives[$menu->id()]['admin_label'] = $admin_label;
      $this->derivatives[$menu->id()]['config_dependencies']['config'] = [
        $menu->getConfigDependencyName(),
      ];
    }
    return $this->derivatives;
  }

}
