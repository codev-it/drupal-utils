<?php

namespace Drupal\codev_utils\Plugin\Block;

use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Todo.php
 * .
 */

/**
 * Class CategoriesCombinedMenu.
 *
 * Provides Categories combined menu block.
 *
 * @Block(
 *   id = "todo",
 *   admin_label = @Translation("Todo helper"),
 *   category = @Translation("Helper")
 * )
 *
 * @noinspection PhpUnused
 */
class Todo extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Current user
   *
   * @var AccountProxyInterface
   */
  private AccountProxyInterface $user;

  /**
   * @var string
   */
  private string $prefix = '@TODO';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ContainerFactoryPluginInterface|Todo|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    /** @noinspection PhpFieldAssignmentTypeMismatchInspection */
    $instance->user = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $config = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state);
    $label = str_replace($this->prefix, '', $config['label']);
    $form['label']['#default_value'] = trim($label);
    $form['label_display']['#access'] = $this->user->hasPermission('administrator');
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnused
   */
  function blockSubmit($form, FormStateInterface $form_state): void {
    if (!$form_state->getErrors()) {
      $this->configuration['label'] = sprintf('%s %s', $this->prefix,
        $form_state->getValue('label'));
      $this->configuration['label_display'] = 'visible';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return ['#markup' => ''];
  }

}
