<?php

namespace Drupal\codev_utils\Plugin\Block;

use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\system\Plugin\Block\SystemMenuBlock;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AccountMenuModal.php
 * .
 */

/**
 * Class AccountMenuModal.
 *
 * Provides a 'Account Menu Modal' block.
 *
 * @Block(
 *   id = "account_menu_modal",
 *   admin_label = @Translation("User account menu modal"),
 *   category = @Translation("Menus"),
 *   deriver = "Drupal\codev_utils\Plugin\Derivative\AccountMenuModal",
 *   forms = {
 *     "settings_tray" = "\Drupal\system\Form\SystemMenuOffCanvasForm",
 *   },
 * )
 */
class AccountMenuModal extends SystemMenuBlock implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = parent::build();
    $build['#attached']['library'][] = 'core/jquery.form';
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $build['#theme'] = 'menu__account__modal';
    if ($build['#items']) {
      foreach ($build['#items'] as $item) {
        /** @var Url $url */
        $url = $item['url'];
        $accepted = ['user.login', 'user.register'];
        if (in_array($url->getRouteName(), $accepted)) {
          $attr = $url->getOption('attributes') ?? [];
          Utils::appendDialogModalAttr($attr);
          $url->setOption('attributes', $attr);
        }
      }
    }
    return $build;
  }

}
