<?php

namespace Drupal\codev_utils\Plugin\QueueWorker;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Executes interface translation queue tasks.
 *
 * @QueueWorker(
 *   id = "generate_image_style",
 *   title = @Translation("Generate image styles"),
 *   cron = {"time" = 60}
 * )
 *
 * @noinspection PhpUnused
 */
class GenerateImageStyle extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The image style entity storage.
   *
   * @var ImageStyleStorage
   */
  protected ImageStyleStorage $imageStyleStorage;

  /**
   * {@inheritdoc}
   *
   * @return GenerateImageStyle|ContainerFactoryPluginInterface
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): GenerateImageStyle|ContainerFactoryPluginInterface|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->imageStyleStorage = $container->get('entity_type.manager')
      ->getStorage('image_style');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    /** @var File $entity */
    $entity = $data['entity'];
    $styles = $this->imageStyleStorage->loadMultiple();
    $image_uri = $entity->getFileUri();
    /** @var ImageStyle $style */
    foreach ($styles as $style) {
      $destination = $style->buildUri($image_uri);
      $style->createDerivative($image_uri, $destination);
    }
  }

}
