<?php

namespace Drupal\codev_utils\Form;

use Drupal\codev_utils\Helper\Utils;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: AdminSettingsForm.php
 * .
 */

/**
 * Class AdminSettingsForm.
 *
 * @noinspection PhpUnused
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codev_utils.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codev_utils_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config($this->getEditableConfigNames()[0]);
    $data = $config->getRawData();

    $form['login'] = [
      '#type'  => 'details',
      '#title' => t('Login'),
      '#open'  => TRUE,
    ];

    $form['login']['login_ajax_throbber'] = [
      '#type'          => 'select',
      '#title'         => t('Ajax login throbber type:'),
      '#options'       => [
        'throbber'   => 'throbber',
        'fullscreen' => 'fullscreen',
      ],
      '#default_value' => Utils::getArrayValue('login_ajax_throbber', $data, 'throbber'),
      '#min'           => 0,
    ];

    $form['dialog'] = [
      '#type'  => 'details',
      '#title' => t('Modal'),
      '#open'  => TRUE,
    ];

    $form['dialog']['dialog_width'] = [
      '#type'          => 'number',
      '#title'         => t('Default dialog modal window width'),
      '#default_value' => Utils::getArrayValue('dialog_width', $data, ''),
      '#min'           => 0,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    $config = $this->config($this->getEditableConfigNames()[0]);
    $values = $form_state->getValues();

    $config->set('dialog_width', Utils::getArrayValue('dialog_width', $values))
      ->set('login_ajax_throbber', Utils::getArrayValue('login_ajax_throbber', $values))
      ->save();
  }

}
