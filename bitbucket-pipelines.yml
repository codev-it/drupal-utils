# This is a sample build configuration for JavaScript.
# Check our guides at https://confluence.atlassian.com/x/14UWN for more examples.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Docker Hub as your build environment.
image: atlassian/default-image:4

pipelines:
  default:
    - step:
        name: Build and Test
        services:
          - docker
        caches:
          - docker
          - composer
        script:
          # Define the path to the contributed module or theme
          - export CONTRIB=$(realpath .)

          # Define the type (modules/themes) and Drupal version (default is 10)
          - export TYPE=modules
          - export VERSION=10

          # Define network and container paths
          - export NETWORK=phpunit
          - export WORKDIR=/opt/drupal
          - export CONTAINER_PATH="/var/www/html/${TYPE}/${CONTRIB##*/}"

          # Check if the network already exists, if not, create it
          - docker network create $NETWORK

          # Check if the MySQL container exists, if not, create it
          - |
            docker run -d --rm --name db \
                --network $NETWORK \
                -e MYSQL_DATABASE=drupal \
                -e MYSQL_USER=drupal \
                -e MYSQL_PASSWORD=drupal \
                -e MYSQL_ROOT_PASSWORD=root \
                mariadb:11

          # Check if the Drupal container exists, if not, create it
          - |
            docker run -d --rm --name drupal \
                --network $NETWORK \
                -v "${CONTRIB}:${CONTAINER_PATH}" \
                -e PHP_MEMORY_LIMIT=-1 \
                "drupal:${VERSION}"

          # Install jq and git inside the Drupal container
          - docker exec -i drupal sh -c 'apt update && apt install -y jq git'

          # Configure PHPUnit and update composer
          - |
            docker exec -i drupal sh <<EOF
              # Set up PHPUnit configuration
              cp "${CONTAINER_PATH}/phpunit.xml.dist" "${WORKDIR}/phpunit.xml.dist"
              sed -i 's#<directory>./tests</directory>#<directory>'"${CONTAINER_PATH}"'/tests</directory>#' "${WORKDIR}/phpunit.xml.dist"

              # Include Drupal requirements in composer.json
              test -f "${WORKDIR}/composer.json.orig" || mv "${WORKDIR}/composer.json" "${WORKDIR}/composer.json.orig"
              jq -s '.[0] * .[1]' "${WORKDIR}/composer.json.orig" "${CONTAINER_PATH}/composer.json" > "${WORKDIR}/composer.json"

              # Set minimum stability to "dev" in composer.json
              jq '.["minimum-stability"] = "dev"' "${WORKDIR}/composer.json" > "${WORKDIR}/composer.json.tmp"
              mv "${WORKDIR}/composer.json.tmp" "${WORKDIR}/composer.json"

              # Update composer
              composer update
              composer require --dev drupal/core-dev
              composer require --dev phpspec/prophecy-phpunit:^2
            EOF

          # Run PHPUnit tests with the www-data user inside the Drupal container
          - docker exec -i -u www-data drupal phpunit --debug --stop-on-failure
