<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains unit tests for the User helper class in the Codev-IT Drupal module.
 *
 * This file defines unit tests for testing the functionality of the User
 * helper class, which provides utilities for managing and accessing user
 * roles in a Drupal installation. The tests focus on the methods
 * getAllRoles and the sorting function used within the User class.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Kernel\Helper;

use Drupal;
use Drupal\codev_utils\Helper\User;
use Drupal\codev_utils\Helper\Utils;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Kernel tests for the User helper class.
 *
 * Tests the functionality of the User helper class by verifying the output
 * of its methods, specifically focusing on retrieving and sorting user roles.
 * These tests ensure that user roles are accurately retrieved and sorted,
 * reflecting the correct hierarchy and order as expected in a Drupal
 * environment.
 */
class UserTest extends KernelTestBase {

  use UserCreationTrait;

  /**
   * @var Role|null
   */
  public ?Role $role;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'test.settings',
    'redirect_after_login.settings',
    'views.view.test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('user');

    $this->role = Role::create(['id' => 'role', 'label' => 'role']);
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->role->save();
  }

  /**
   * Test: User::getAllRoles
   */
  public function testGetAllRoles() {
    $this->assertEquals([], User::getAllRoles(TRUE));

    /** @noinspection PhpUnhandledExceptionInspection */
    Drupal::currentUser()->setAccount($this->createUser());
    $this->assertEquals([
      'anonymous',
    ], array_keys(User::getAllRoles()));

    $this->assertEquals([
      'anonymous',
      'authenticated',
    ], array_keys(User::getAllRoles(TRUE)));

    /** @noinspection PhpUnhandledExceptionInspection */
    $user = $this->createUser();
    $user->addRole('role');
    Drupal::currentUser()->setAccount($user);
    $this->assertEquals([
      'anonymous',
      'authenticated',
    ], array_keys(User::getAllRoles()));

    $this->assertEquals([
      'anonymous',
      'authenticated',
      'role',
    ], array_keys(User::getAllRoles(TRUE)));

    $this->role->setWeight(-99);
    /** @noinspection PhpUnhandledExceptionInspection */
    $this->role->save();
    $this->assertEquals([
      'role',
      'anonymous',
    ], array_keys(User::getAllRoles()));

    $this->assertEquals([
      'role',
      'anonymous',
      'authenticated',
    ], array_keys(User::getAllRoles(TRUE)));
  }

  /**
   * Test: User::getAllRoles
   *
   * This test only the sort segment form the getAllRoles functions.
   *
   * @noinspection PhpUnhandledExceptionInspection
   */
  public function testGetAllRolesSorting() {
    // create additional roles
    Role::create(['id' => 'role2', 'label' => 'role2'])->save();
    Role::create(['id' => 'role3', 'label' => 'role3'])->save();
    Role::create(['id' => 'role4', 'label' => 'role4'])->save();
    Role::create(['id' => 'role5', 'label' => 'role5'])->save();

    // shuffle roles
    $shuffle_roles = [];
    $roles = Role::loadMultiple();
    $roles_keys = array_keys($roles);
    shuffle($roles_keys);
    foreach ($roles_keys as $key) {
      $shuffle_roles[$key] = $roles[$key];
    }

    // test sort
    Utils::sortObjectByMethod($shuffle_roles);
    $this->assertEquals([
      'anonymous',
      'authenticated',
      'role',
      'role2',
      'role3',
      'role4',
      'role5',
    ], array_keys($shuffle_roles));
  }

}
