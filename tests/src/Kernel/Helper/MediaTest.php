<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains unit tests for the Media helper class in the Codev-IT Drupal
 * module.
 *
 * This file defines unit tests for the Media helper class, focusing on methods
 * that manipulate and generate URLs and renderable arrays for media items and
 * images.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Kernel\Helper;

use Drupal;
use Drupal\codev_utils\Helper\Media;
use Drupal\codev_utils_test\Settings;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media as MediaEntity;
use Exception;

/**
 * Kernel tests for the Media helper class.
 *
 * Tests the functionality of the Media helper class by verifying the
 * correctness of the URLs and renderable arrays generated for media entities
 * and images.
 */
class MediaTest extends KernelTestBase {

  /**
   * @var string|null
   */
  public ?string $filePath;

  /**
   * @var FileSystem|null
   */
  public ?FileSystem $fileSystem;

  /**
   * @var FileUrlGenerator|null
   */
  public ?FileUrlGenerator $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'codev_utils_test',
    'image',
    'media',
    'file',
    'field',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'test.settings',
    'media.type.test',
    'views.view.test',
    'redirect_after_login.settings',
    'field.field.media.test.field_media_image',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installSchema('file', ['file_usage']);
    $this->installSchema('system', ['sequences']);

    $this->installEntitySchema('field_storage_config');
    $this->installEntitySchema('field_config');

    $this->installConfig(['media', 'codev_utils_test']);

    $this->filePath = Settings::modulePath() . '/assets/images';
    $this->fileSystem = Drupal::service('file_system');
    $this->fileUrlGenerator = Drupal::service('file_url_generator');
  }

  /**
   * Tests the getUrlFromFile method.
   *
   * Verifies that the method correctly generates URLs for files.
   */
  public function testGetUrlFromFile() {
    $file = $this->createFile();
    $this->assertNotNull($file);

    // test relative
    $file_url = $this->fileUrlGenerator->generateString($file->getFileUri());
    $url = Media::getUrlFromFile($file);
    $this->assertEquals($file_url, $url);

    // test absolute
    $file_url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    $url = Media::getUrlFromFile($file, '', FALSE);
    $this->assertEquals($file_url, $url);
  }

  /**
   * Create file.
   *
   * @return File|null
   */
  private function createFile(): ?File {
    try {
      /** @var FileSystem $file_system */
      $file_system = Drupal::service('file_system');
      $image_path = $this->filePath . '/bookmark.svg';
      $uri = $file_system->copy($image_path, 'public://bookmark.svg', FileExists::Replace);
      $file = File::create(['uri' => $uri]);
      $file->save();
      return $file;
    } catch (Exception) {
      return NULL;
    }
  }

  /**
   * Tests the getUrlByFileId method.
   *
   * Verifies that the method correctly generates URLs for files using their
   * IDs.
   */
  public function testGetUrlByFileId() {
    $file = $this->createFile();
    $this->assertNotNull($file);

    // test relative
    $file_url = $this->fileUrlGenerator->generateString($file->getFileUri());
    $url = Media::getUrlByFileId($file->id());
    $this->assertEquals($file_url, $url);

    // test absolute
    $file_url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    $url = Media::getUrlByFileId($file->id(), '', FALSE);
    $this->assertEquals($file_url, $url);
  }

  /**
   * Tests the getUrlByMedia method.
   *
   * Verifies that the method correctly generates URLs for media entities.
   */
  public function testGetUrlByMedia() {
    $file = $this->createFile();
    $media = $this->createMedia($file);
    $this->assertNotNull($file);
    $this->assertNotNull($media);

    // test relative
    $file_url = $this->fileUrlGenerator->generateString($file->getFileUri());
    $url = Media::getUrlByMedia($media);
    $this->assertEquals($file_url, $url);

    // test absolute
    $file_url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    $url = Media::getUrlByMedia($media, '', FALSE);
    $this->assertEquals($file_url, $url);
  }

  /**
   * Create file.
   *
   * @param File $file
   *
   * @return MediaEntity|null
   */
  private function createMedia(File $file): ?MediaEntity {
    try {
      $media = MediaEntity::create([
        'bundle'            => 'test',
        'name'              => 'Test',
        'field_media_image' => [
          'target_id' => $file->id(),
          'alt'       => 'Test',
        ],
      ]);
      $media->save();
      return $media;
    } catch (Exception) {
      return NULL;
    }
  }

  /**
   * Tests the getUrlByMediaId method.
   *
   * Verifies that the method correctly generates URLs for media entities using
   * their IDs.
   */
  public function testGetUrlByMediaId() {
    $file = $this->createFile();
    $media = $this->createMedia($file);
    $this->assertNotNull($file);
    $this->assertNotNull($media);

    // test relative
    $file_url = $this->fileUrlGenerator->generateString($file->getFileUri());
    $url = Media::getUrlByMediaId($media->id());
    $this->assertEquals($file_url, $url);

    // test absolute
    $file_url = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    $url = Media::getUrlByMediaId($media->id(), '', FALSE);
    $this->assertEquals($file_url, $url);
  }

  /**
   * Tests the renderableImage method.
   *
   * Verifies that the method correctly generates a renderable array for images.
   */
  public function testRenderableImage() {
    $accept = [
      '#theme'      => 'image_style',
      '#style_name' => 'thumbnail',
      '#uri'        => 'test.png',
      '#alt'        => 'Alt',
      '#title'      => 'Title',
      '#width'      => 10,
      '#height'     => 10,
      '#weight'     => 1,
    ];

    /** @var File $file */
    $file = File::create(['uri' => 'test.png']);
    $test = Media::renderableImage($file, 'thumbnail', [
      'alt'    => 'Alt',
      'title'  => 'Title',
      'width'  => 10,
      'height' => 10,
      'weight' => 1,
    ]);
    ksort($accept);
    ksort($test);
    $this->assertEquals($test, $accept);
  }

  /**
   * Tests the buildBase64 method.
   *
   * Verifies that the method correctly generates a base64 encoded string for
   * images.
   */
  public function testBuildBase64() {
    $image_path = $this->filePath . '/bookmark.svg';
    $base64_data = base64_encode(file_get_contents($image_path));
    $base64_accepted = 'data:image/svg+xml;base64,' . $base64_data;
    $file = $this->createFile();
    $this->assertNotNull($file);
    $this->assertEquals($base64_accepted, Media::buildBase64($file));
  }

  /**
   * Tests the emptyImageMarkup method.
   *
   * Verifies that the method correctly generates an empty image markup with
   * specified dimensions.
   *
   * @throws EntityStorageException
   */
  public function testEmptyImageMarkup() {
    // Erstellen Sie einen Bildstil mit einem bestimmten Effekt.
    $style = ImageStyle::create([
      'name'  => 'test_style',
      'label' => 'Test Style',
    ]);
    $style->addImageEffect([
      'id'   => 'image_scale',
      'data' => ['width' => 100, 'height' => 100],
    ]);
    $style->save();

    // Führen Sie die zu testende Methode aus.
    $result = Media::emptyImageMarkupByStyle('test_style');

    // Führen Sie Assertions durch, um das Ergebnis zu überprüfen.
    $this->assertEquals(100, $result['#width']);
    $this->assertEquals(100, $result['#height']);
  }

}
