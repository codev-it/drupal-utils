<?php

/** @noinspection PhpUnused */

namespace Drupal\Tests\codev_utils\Kernel;

/**
 * @file
 * Contains unit tests for the Utils class in the Codev-IT Drupal project.
 *
 * This file includes the UtilsTest class, which provides unit tests for the
 * Utils class methods. These tests ensure the functionality and reliability of
 * the utilities provided by the Utils class in the Drupal Codev-IT project.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

use Drupal\codev_utils\Helper\Utils;
use Drupal\codev_utils_test\Settings;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactory;
use Drupal\KernelTests\KernelTestBase;

/**
 * Unit tests for the Utils class.
 *
 * This class provides a set of tests to verify the functionality of the methods
 * in the Utils class. It covers various utility methods, including modal dialog
 * attributes appending, translation extraction, encryption, and decryption.
 */
class UtilsTest extends KernelTestBase {

  /**
   * @var ConfigFactory|null
   */
  protected ConfigFactory|null $configFactory;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'codev_utils_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'redirect_after_login.settings',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->configFactory = $this->container->get('config.factory');
  }

  /**
   * Tests the appendDialogModalAttr method in Utils class.
   *
   * Verifies that the method correctly appends modal dialog attributes to an
   * array. Tests both with a new array and with an existing array to ensure
   * attributes are properly merged and added.
   */
  public function testAppendDialogModalAttr() {
    $attr = [];
    $opts = ['width' => 500];
    Utils::appendDialogModalAttr($attr, $opts);
    $this->assertContains('use-ajax', $attr['class']);
    $this->assertEquals('modal', $attr['data-dialog-type']);
    $this->assertEquals(Json::encode($opts), $attr['data-dialog-options']);

    // Test with an existing array.
    $attr = ['class' => ['existing-class'], 'other-attr' => 'value'];
    $opts = ['width' => 300];
    Utils::appendDialogModalAttr($attr, $opts);
    $this->assertContains('existing-class', $attr['class']);
    $this->assertContains('use-ajax', $attr['class']);
    $this->assertEquals('modal', $attr['data-dialog-type']);
    $this->assertEquals(Json::encode($opts), $attr['data-dialog-options']);
    $this->assertEquals('value', $attr['other-attr']);
  }

  /**
   * Tests the poT method in Utils class for translation extraction.
   *
   * Verifies that the method correctly extracts translations from .po files.
   * Tests with different language files to ensure the method works with various
   * translation sources.
   */
  public function testPoT() {
    $module_path = Settings::modulePath();
    $file_en = $module_path . '/translations/en.po';
    $file_de = $module_path . '/translations/de.po';

    $this->assertEquals('Test', Utils::poT('Test', ''));

    $this->assertEquals('Test', Utils::poT('Test', $file_en));
    $this->assertEquals('Test translation en', Utils::poT('Test source en', $file_en));

    $this->assertEquals('Test', Utils::poT('Test', $file_de));
    $this->assertEquals('Test translation de', Utils::poT('Test source de', $file_de));
  }

  /**
   * Tests the encrypt method in Utils class.
   *
   * Ensures that the encryption method alters the input string and does not
   * return the original unencrypted string.
   */
  public function testEncrypt() {
    $this->assertNotEquals('Test', Utils::encrypt('Test'));
  }

  /**
   * Tests the decrypt method in Utils class.
   *
   * Verifies that the decrypt method correctly reverses the encryption process
   * and returns the original string.
   */
  public function testDecrypt() {
    $hash = Utils::encrypt('Test');
    $this->assertEquals('Test', Utils::decrypt($hash));
  }

}
