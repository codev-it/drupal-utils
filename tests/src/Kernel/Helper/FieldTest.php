<?php /** @noinspection PhpUnused */

/**
 * @file
 * Contains kernel tests for the Field utility class in Drupal.
 *
 * This file defines the FieldTest class, which includes kernel tests for
 * testing the functionality of the Field utility class. These tests ensure that
 * the Field class correctly handles field data extraction, field creation, and
 * other field-related operations.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Kernel\Helper;

use Drupal\codev_utils\Helper\Field;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;

/**
 * Kernel tests for the Field utility class.
 *
 * This class provides kernel tests for the Field utility class, which offers
 * utility functions for handling fields in Drupal. The tests cover methods for
 * field data extraction, field creation, and other field-related operations.
 */
class FieldTest extends KernelTestBase {

  /**
   * @var EntityBase|Node|EntityInterface|ContentEntityBase
   */
  private EntityBase|Node|EntityInterface|ContentEntityBase $node;

  /**
   * @var EntityBase|Node|EntityInterface|ContentEntityBase
   */
  private EntityBase|Node|EntityInterface|ContentEntityBase $node2;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'field',
    'node',
    'text',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUnhandledExceptionInspection
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'user']);
    $this->installSchema('system', ['sequences']);
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    NodeType::create(['type' => 'test'])->save();
    NodeType::create(['type' => 'add_field'])->save();

    FieldStorageConfig::create([
      'field_name'  => 'field_test_string',
      'entity_type' => 'node',
      'type'        => 'string',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();

    FieldConfig::create([
      'field_name'  => 'field_test_string',
      'entity_type' => 'node',
      'bundle'      => 'test',
      'label'       => 'Test string',
    ])->save();

    FieldStorageConfig::create([
      'field_name'  => 'field_test_text',
      'entity_type' => 'node',
      'type'        => 'text_long',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
    ])->save();

    FieldConfig::create([
      'field_name'  => 'field_test_text',
      'entity_type' => 'node',
      'bundle'      => 'test',
      'label'       => 'Test text',
    ])->save();

    FieldStorageConfig::create([
      'field_name'  => 'field_test_entity_reference',
      'entity_type' => 'node',
      'type'        => 'entity_reference',
      'cardinality' => FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
      'settings'    => [
        'target_type' => 'node',
      ],
    ])->save();

    FieldConfig::create([
      'field_name'  => 'field_test_entity_reference',
      'entity_type' => 'node',
      'bundle'      => 'test',
      'label'       => 'Text text',
    ])->save();

    $this->node = Node::create([
      'title' => t('Test'),
      'type'  => 'test',
    ]);
    $this->node->save();

    $this->node2 = Node::create([
      'title' => t('Test'),
      'type'  => 'test',
    ]);
    $this->node2->save();
  }

  /**
   * Tests Field::fmtCleanValue method for field value extraction.
   *
   * @throws EntityStorageException
   */
  public function testFmtCleanValue() {
    $node = Node::create([
      'title'             => t('Test'),
      'type'              => 'test',
      'field_test_string' => '',
    ]);
    $node->save();

    $this->assertEquals('empty', Field::fmtCleanValue('field_test_string', $node, 'empty'));
    $this->assertEquals(['empty'], Field::fmtCleanValue('field_test_string', $node, ['empty']));

    $node = Node::create([
      'title'                       => t('Test'),
      'type'                        => 'test',
      'field_test_string'           => 'string',
      'field_test_text'             => 'text',
      'field_test_entity_reference' => $this->node,
    ]);
    $node->save();

    $this->assertEquals('string', Field::fmtCleanValue('field_test_string', $node));
    $this->assertEquals('text', Field::fmtCleanValue('field_test_text', $node));
    $this->assertEquals($this->node->id(), Field::fmtCleanValue('field_test_entity_reference', $node, NULL, TRUE, 'target_id'));

    $this->assertEquals(['string'], Field::fmtCleanValue('field_test_string', $node, NULL, FALSE));
    $this->assertEquals(['text'], Field::fmtCleanValue('field_test_text', $node, NULL, FALSE));
    $this->assertEquals([$this->node->id()], Field::fmtCleanValue('field_test_entity_reference', $node, NULL, FALSE, 'target_id'));

    $node = Node::create([
      'label'                       => t('Test'),
      'type'                        => 'test',
      'field_test_string'           => ['string', 'string2'],
      'field_test_text'             => ['text', 'text2'],
      'field_test_entity_reference' => [$this->node, $this->node2],
    ]);

    $this->assertEquals([
      'string',
      'string2',
    ], Field::fmtCleanValue('field_test_string', $node, NULL, FALSE));
    $this->assertEquals([
      'text',
      'text2',
    ], Field::fmtCleanValue('field_test_text', $node, NULL, FALSE));
  }

  /**
   * Tests Field::fmtCleanValues method for multiple field value extraction.
   */
  public function testFmtCleanValues() {
    $node = Node::create([
      'label'           => t('Test'),
      'type'            => 'test',
      'field_test_text' => [
        'value'  => 'text',
        'format' => 'basic_html',
      ],
    ]);

    $this->assertEquals([
      'value'  => 'text',
      'format' => 'basic_html',
    ], Field::fmtCleanValues('field_test_text', $node, NULL, TRUE, [
      'value',
      'format',
    ]));

    $node = Node::create([
      'label'           => t('Test'),
      'type'            => 'test',
      'field_test_text' => [
        [
          'value'  => 'text',
          'format' => 'basic_html',
        ],
        [
          'value'  => 'text2',
          'format' => 'basic_html',
        ],
      ],
    ]);

    $this->assertEquals([
      [
        'value'  => 'text',
        'format' => 'basic_html',
      ],
      [
        'value'  => 'text2',
        'format' => 'basic_html',
      ],
    ], Field::fmtCleanValues('field_test_text', $node, NULL, FALSE, [
      'value',
      'format',
    ]));
  }

  /**
   * Tests Field::addToEntity method for adding fields to entities.
   *
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   * @throws EntityStorageException
   * @throws MissingDataException
   */
  public function testAddToEntity() {
    $node = Node::create([
      'title'             => t('Test'),
      'type'              => 'add_field',
      'field_test_string' => '',
    ]);
    $this->assertFalse($node->hasField('field_test'));

    Field::addToEntity([
      'label'       => t('Test field'),
      'entity_type' => 'node',
      'bundle'      => 'add_field',
      'field_name'  => 'field_test',
      'field_type'  => 'string',
    ]);

    /** @var Node $node */
    $node = Node::create([
      'title'             => t('Test'),
      'type'              => 'add_field',
      'field_test_string' => '',
    ]);
    $this->assertTrue($node->hasField('field_test'));

    Field::addToEntity([
      'label'       => t('Test field'),
      'entity_type' => 'node',
      'bundle'      => 'add_field',
      'field_name'  => 'field_test',
      'field_type'  => 'string',
    ]);

    $field_definitions = $node->getFieldDefinition('field_test');
    $this->assertEquals(t('Test field'), $field_definitions->getLabel());
    $this->assertEquals('string', $field_definitions->getType());
  }

}
