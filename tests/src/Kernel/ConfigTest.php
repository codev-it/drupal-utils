<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains unit tests for the Config utility class in the Drupal Codev-IT
 * project.
 *
 * This file includes the ConfigTest class, which provides unit tests for the
 * Config utility class methods. These tests ensure the functionality and
 * reliability of the configuration management provided by the Config class in
 * the Drupal Codev-IT project.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Kernel;

use Drupal;
use Drupal\codev_utils\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Serialization\Yaml;
use Drupal\KernelTests\KernelTestBase;
use Drupal\views\Entity\View;
use Exception;

/**
 * Unit tests for the Config utility class.
 *
 * This class provides a set of tests to verify the functionality of the
 * methods
 * in the Config utility class. It covers methods for overriding configuration,
 * cloning and removing views display modes, and setting user redirect paths
 * after login.
 */
class ConfigTest extends KernelTestBase {

  /**
   * @var ConfigFactory|null
   */
  protected ConfigFactory|null $configFactory;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'codev_utils_test',
    'language',
    'file',
    'user',
    'system',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'test.settings',
    'media.type.test',
    'views.view.test',
    'redirect_after_login.settings',
    'field.field.media.test.field_media_image',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('file');
    $this->installConfig(['codev_utils_test', 'user']);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->configFactory = $this->container->get('config.factory');
  }

  /**
   * Tests the override method in Config class.
   *
   * Verifies that the method correctly overrides configuration settings from a
   * specified module. Tests before and after states to ensure that the
   * configuration is altered as expected.
   *
   * @throws Exception
   */
  public function testOverride() {
    $module_path = Drupal::moduleHandler()
      ->getModule('codev_utils_test')
      ->getPath();

    /** @noinspection MissingService */
    $test_config_before = $this->configFactory->get('test.settings')
      ->getRawData();
    Config::override('test.settings', 'codev_utils_test');
    /** @noinspection MissingService */
    $test_config_after = $this->configFactory->get('test.settings')
      ->getRawData();
    $this->assertNotEquals($test_config_before, $test_config_after);

    unset($test_config_after['_core']);
    $settings_file = sprintf('%s/config/optional/%s.yml', $module_path, 'test.settings');
    $raw_settings = Yaml::decode(file_get_contents($settings_file));
    $this->assertEquals($raw_settings, $test_config_after);
  }

  /**
   * Tests the overrideTranslation method in Config class.
   *
   * Ensures that the method correctly overrides translation settings from a
   * specified module. Verifies the changes in configuration after the
   * override.
   *
   * @throws Exception
   */
  public function testOverrideTranslation() {
    $module_path = Drupal::moduleHandler()
      ->getModule('codev_utils_test')
      ->getPath();

    /** @noinspection MissingService */
    $test_config_before = $this->configFactory->get('test.settings')
      ->getRawData();
    Config::overrideTranslation('test.settings', 'codev_utils_test', ['en']);
    /** @noinspection MissingService */
    $test_config_after = $this->configFactory->get('test.settings')
      ->getRawData();
    $this->assertNotEquals($test_config_before, $test_config_after);

    unset($test_config_after['_core']);
    $settings_file = sprintf('%s/config/optional/%s.yml', $module_path, 'test.settings');
    $raw_settings = Yaml::decode(file_get_contents($settings_file));
    $this->assertEquals($raw_settings, $test_config_after);
  }

  /**
   * Tests the cloneViewsDisplayMode method in Config class.
   *
   * Verifies the functionality of cloning a display mode in a view.
   * Ensures that the new display mode is added as expected.
   */
  function testCloneViewsDisplayMode() {
    $view = View::load('test')->toArray();
    /** @noinspection PhpUnitAssertCanBeReplacedWithEmptyInspection */
    $this->assertTrue(empty($view['display']['test2']));

    Config::cloneViewsDisplayMode('test', 'test', [
      'id' => 'test2',
    ]);

    $view = View::load('test')->toArray();
    /** @noinspection PhpUnitAssertCanBeReplacedWithEmptyInspection */
    $this->assertFalse(empty($view['display']['test2']));
  }

  /**
   * Tests the removeViewsDisplayMode method in Config class.
   *
   * Confirms that the method can successfully remove a specified display mode
   * from a view.
   */
  function testRemoveViewsDisplayMode() {
    $view = View::load('test')->toArray();
    /** @noinspection PhpUnitAssertCanBeReplacedWithEmptyInspection */
    $this->assertFalse(empty($view['display']['test']));

    /** @noinspection PhpUnhandledExceptionInspection */
    Config::removeViewsDisplayMode('test', 'test');

    $view = View::load('test')->toArray();
    /** @noinspection PhpUnitAssertCanBeReplacedWithEmptyInspection */
    $this->assertTrue(empty($view['display']['test']));
  }

  /**
   * Tests the setUserRedirectAfterLogin method in Config class.
   *
   * Checks whether the method correctly sets the redirect path for a user role
   * after login. Compares configuration states before and after the method
   * call.
   *
   * @throws Exception
   */
  function testSetUserRedirectAfterLogin() {
    /** @noinspection MissingService */
    $test_config_before = $this->configFactory
      ->get('redirect_after_login.settings')
      ->getRawData();

    Config::setUserRedirectAfterLogin('authenticated', '/test');
    /** @noinspection MissingService */
    $test_config_after = $this->configFactory
      ->get('redirect_after_login.settings')
      ->getRawData();

    $this->assertNotEquals($test_config_before, $test_config_after);
    $test_config_before['login_redirection']['authenticated'] = '/test';
    $this->assertEquals($test_config_before, $test_config_after);
  }

}
