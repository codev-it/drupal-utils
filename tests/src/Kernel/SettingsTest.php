<?php

/** @noinspection PhpUnused */

/**
 * @file
 * Contains unit tests for the Settings utility class in the Drupal Codev-IT
 * project.
 *
 * This file includes the SettingsTest class, which provides unit tests for the
 * Settings utility class methods. These tests ensure the functionality and
 * reliability of the settings management provided by the Settings class in the
 * Drupal Codev-IT project.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Kernel;

use Drupal\codev_utils_test\Settings;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Unit tests for the Settings utility class.
 *
 * This class provides a set of tests to verify the functionality of the
 * methods
 * in the Settings utility class. It covers methods for accessing and modifying
 * settings, converting settings to arrays, and retrieving module paths.
 */
class SettingsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'codev_utils_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $configSchemaCheckerExclusions = [
    'test.settings',
    'media.type.test',
    'views.view.test',
    'redirect_after_login.settings',
    'field.field.media.test.field_media_image',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['codev_utils_test']);
  }

  /**
   * Tests the getReadonly method in Settings class.
   *
   * Verifies that the method correctly retrieves readonly configuration
   * settings.
   */
  public function testGetReadonly() {
    $this->assertTrue(Settings::getReadonly() instanceof ImmutableConfig);
  }

  /**
   * Tests the getEditable method in Settings class.
   *
   * Ensures that the method correctly retrieves editable configuration
   * settings.
   */
  public function testGetEditable() {
    $this->assertTrue(Settings::getEditable() instanceof Config);
  }

  /**
   * Tests the toArray method in Settings class.
   *
   * Confirms that the method correctly converts configuration settings to an
   * array, with the option to exclude specific keys.
   */
  public function testToArray() {
    $this->assertEquals([
      'langcode'         => 'en',
      'int'              => 123,
      'string'           => 'String val',
      'sequence'         => ['value'],
      'sequence_array'   => [['key' => 'value']],
      'sequence_mapping' => ['mapping' => ['key' => 'value']],
    ], Settings::toArray());

    $this->assertEquals([
      'langcode'       => 'en',
      'string'         => 'String val',
      'sequence_array' => [['key' => 'value']],
    ], Settings::toArray(['int', 'sequence', 'sequence_mapping']));
  }

  /**
   * Tests the set method in Settings class.
   *
   * Verifies the functionality of setting a new value for a configuration
   * setting.
   */
  public function testSet() {
    $this->assertTrue(Settings::set('int', 456));
    $this->assertEquals(456, Settings::get('int'));

    $this->assertTrue(Settings::set('string', 'New string'));
    $this->assertEquals('New string', Settings::get('string'));

    $this->assertTrue(Settings::set('sequence', ['value']));
    $this->assertEquals(['value'], Settings::get('sequence'));

    $this->assertTrue(Settings::set('sequence_array', [['key' => 'value']]));
    $this->assertEquals([['key' => 'value']], Settings::get('sequence_array'));

    $this->assertTrue(Settings::set('sequence_mapping', ['mapping' => ['key' => 'value']]));
    $this->assertEquals(['mapping' => ['key' => 'value']], Settings::get('sequence_mapping'));
  }

  /**
   * Tests the get method in Settings class.
   *
   * Checks whether the method correctly retrieves the value for a given
   * configuration setting.
   */
  public function testGet() {
    $this->assertEquals(123, Settings::get('int'));
    $this->assertEquals('String val', Settings::get('string'));
    $this->assertEquals(['value'], Settings::get('sequence'));
    $this->assertEquals([['key' => 'value']], Settings::get('sequence_array'));
    $this->assertEquals(['mapping' => ['key' => 'value']], Settings::get('sequence_mapping'));
  }

  /**
   * Tests the modulePath method in Settings class.
   *
   * Verifies that the method correctly retrieves the file system path of the
   * module.
   */
  public function testModulePath() {
    $accepted = realpath(dirname(__FILE__) . '/../../modules/codev_utils_test');
    $this->assertEquals($accepted, Settings::modulePath());
  }

}
