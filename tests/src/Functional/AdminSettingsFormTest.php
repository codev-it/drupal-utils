<?php

/**
 * @file
 * Contains functional tests for the admin settings form of the codev_utils
 * module in Drupal.
 *
 * This file provides a functional test class for testing the administration
 * settings form of the codev_utils module. It tests various aspects of the
 * form, including access control, form elements, and the form submission
 * process.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests the functionality of the admin settings form in the codev_utils module.
 *
 * This test class performs functional testing of the admin settings form
 * provided by the codev_utils module. It checks for access permissions, form
 * element existence, and the ability to submit the form.
 */
class AdminSettingsFormTest extends FunctionalTestBase {

  /**
   * The admin user used for testing.
   *
   * @var User|UserInterface|false
   *   A user entity with administrative permissions for testing.
   */
  protected User|UserInterface|false $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser([
      'administer utils',
    ]);
  }

  /**
   * Tests the admin settings form for functionality and access control.
   *
   * Performs various checks on the admin settings form to ensure that it is
   * functioning correctly, including access control, presence of form elements,
   * and form submission.
   *
   * @throws ExpectationException
   */
  public function testAdminSettingsForm() {
    $assert = $this->assertSession();

    // No anonymous access
    $this->drupalGet('admin/config/system/codev_utils');
    $assert->statusCodeEquals(403);

    // Admin access
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/system/codev_utils');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains(t('Login'));
    $assert->pageTextContains(t('Ajax login throbber type:'));
    $assert->pageTextContains(t('Modal'));
    $assert->pageTextContains(t('Default dialog modal window width'));
    $this->submitForm([], t('Save configuration'));
  }

}
