<?php

/**
 * @file
 * Contains functional tests for the Codev-IT utilities in the Drupal project.
 *
 * This file includes the CodevUtilsTest class, which provides functional tests
 * to ensure the correct behavior of utilities provided by the Codev-IT
 * project, including permissions, roles, and custom blocks.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Functional;

use Behat\Mink\Exception\ExpectationException;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;


/**
 * Functional tests for Codev-IT utilities.
 *
 * Tests the installation and behavior of various utilities and features in the
 * Codev-IT project, such as permission handling, custom blocks, and role
 * management.
 *
 * @noinspection PhpUnused
 */
class CodevUtilsTest extends FunctionalTestBase {

  /**
   * The admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $adminUser;

  /**
   * The permission admin user.
   *
   * @var UserInterface
   */
  protected UserInterface $permAdminUser;

  /**
   * The role's admin user.
   *
   * @var User|UserInterface|false
   */
  protected User|UserInterface|false $roleAdminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'codev_utils',
    'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_perm = [
      'administer menu',
      'administer blocks',
      'administer permissions',
      'administer basic site settings',
      'view status report',
      'access cron',
      'view status report',
    ];

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->adminUser = $this->drupalCreateUser($admin_perm);

    /** @noinspection PhpUnhandledExceptionInspection */
    $this->roleAdminUser = $this->drupalCreateUser(array_merge($admin_perm, [
      'administer permissions admin',
    ]));
  }

  /**
   * Tests various functionalities of the Codev-IT utilities.
   *
   * Verifies the correct behavior of permissions, roles, and custom blocks.
   * Tests include checking access to permissions edit page, role access and
   * permissions, custom account block, status page access, site settings
   * access, and anonymous user access.
   *
   * @throws ExpectationException
   */
  public function testCodevUtils() {
    $assert = $this->assertSession();

    // Permissions edit page show only permissions with access
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/people/permissions');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('Access run cron');
    $assert->pageTextNotContains('Bypass roles access control');

    $this->drupalLogin($this->roleAdminUser);
    $this->drupalGet('admin/people/permissions');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains('Access run cron');
    $assert->pageTextContains('Bypass roles access control');

    // Role access and permissions
    $admin_roles = $this->adminUser->getRoles();
    $admin_role = Role::load(end($admin_roles));
    $role_admin_roles = $this->adminUser->getRoles();
    $role_admin_role = Role::load(end($role_admin_roles));
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/people/roles');
    $assert->statusCodeEquals(200);
    $assert->pageTextNotContains($admin_role->label());

    $this->drupalLogin($this->roleAdminUser);
    $this->drupalGet('admin/people/roles');
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($role_admin_role->label());

    // Check for custom account block
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/block/add/account_menu_modal:account');
    $assert->statusCodeEquals(200);
    $this->submitForm([
      'region' => 'content',
      'id'     => 'account_menu_modal',
    ], t('Save block'));
    $this->drupalPlaceBlock('account_menu_modal:account');

    // Check for custom toto block
    $this->drupalGet('admin/structure/block/add/todo');
    $assert->statusCodeEquals(200);
    $this->submitForm([
      'region' => 'content',
      'id'     => 'todo',
    ], t('Save block'));
    $this->drupalPlaceBlock('todo');

    // Check status page permission
    $this->drupalGet('admin/reports/status');
    $assert->statusCodeEquals(200);

    // Check basic site settings permission
    $this->drupalGet('admin/config/system/site-information');
    $assert->statusCodeEquals(200);

    // Check status page permission
    $this->drupalGet('admin/people/roles');
    $assert->statusCodeEquals(200);

    // Anonymous tests.
    $this->drupalLogout();
    $this->drupalGet('');
    $assert->pageTextContains('User account menu modal');
    $assert->linkExists(t('Log in'));
    $assert->statusCodeEquals(200);

    // Permissions edit page access
    $this->drupalGet('admin/people/permissions');
    $assert->statusCodeEquals(403);

    // Check status page permission
    $this->drupalGet('admin/reports/status');
    $assert->statusCodeEquals(403);

    // Check basic site settings permission
    $this->drupalGet('admin/config/system/site-information');
    $assert->statusCodeEquals(403);

    // Check run cron permission
    $this->drupalGet('admin/reports/status/run-cron');
    $assert->statusCodeEquals(403);

    // Check status page permission
    $this->drupalGet('admin/people/roles');
    $assert->statusCodeEquals(403);
  }

}
