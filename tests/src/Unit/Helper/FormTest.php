<?php

/**
 * @file
 * Contains unit tests for the Form utility class in Drupal.
 *
 * This file defines the FormTest class, which includes unit tests for
 * testing the functionality of the Form utility class, specifically the
 * method for constructing form item names.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: FormTest.php
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal\codev_utils\Helper\Form;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the Form helper class.
 *
 * This class provides unit tests for the Form helper class, which offers
 * utility functions for handling forms in Drupal. This test focuses on the
 * method for constructing form item names.
 */
class FormTest extends UnitTestCase {

  /**
   * Tests Form::buildFormItemName method for constructing form item names.
   */
  public function testBuildFormItemName() {
    // Test the buildFormItemName method with various input scenarios and validate results.
    $this->assertEquals('test', Form::buildFormItemName(['test']));
    $this->assertEquals('parent[test]', Form::buildFormItemName([
      'parent',
      'test',
    ]));
    $this->assertEquals('parent[0][test]', Form::buildFormItemName([
      'parent',
      0,
      'test',
    ]));
    $this->assertEquals('parent[0][sub][test]', Form::buildFormItemName([
      'parent',
      0,
      'sub',
      'test',
    ]));
  }

}
