<?php

/**
 * @file
 * Contains unit tests for the Field utility class in Drupal.
 *
 * This file defines the FieldTest class, which includes unit tests for
 * testing the functionality of the Field utility class, specifically the
 * method for changing image styles in renderable fields.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal\codev_utils\Helper\Field;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the Field helper class.
 *
 * This class provides unit tests for the Field helper class, which offers
 * utility functions for handling fields in Drupal. This test focuses on the
 * method for changing image styles in renderable fields.
 */
class FieldTest extends UnitTestCase {

  /**
   * Sample field items array before applying the image style change.
   *
   * @var array
   */
  protected array $fieldItemsA = [];

  /**
   * Sample field items array after applying the image style change.
   *
   * @var array
   */
  protected array $fieldItemsB = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up initial field items array and expected results.
    $this->fieldItemsA = [
      [
        '#image_style' => 'default',
      ],
      [
        '#image_style' => 'default',
      ],
    ];

    $this->fieldItemsB = [
      [
        '#image_style' => 'changed',
      ],
      [
        '#image_style' => 'changed',
      ],
    ];
  }

  /**
   * Tests Field::changeImageStyle method for modifying image styles.
   */
  public function testChangeImageStyle() {
    // Test the changeImageStyle method and validate results.
    Field::changeImageStyle($this->fieldItemsA, 'changed');
    $this->assertEquals($this->fieldItemsA, $this->fieldItemsB);
  }

}
