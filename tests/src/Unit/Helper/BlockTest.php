<?php

/**
 * @file
 * Contains unit tests for the Block helper class in Drupal.
 *
 * This file defines the BlockTest class, which includes unit tests for
 * testing the functionality of the Block helper class. These tests ensure that
 * the Block class correctly handles loading and rendering of blocks in Drupal.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal;
use Drupal\block\BlockInterface;
use Drupal\block\BlockViewBuilder;
use Drupal\codev_utils\Helper\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityViewBuilderInterface;
use Drupal\Tests\UnitTestCase;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Unit tests for the Block helper class.
 *
 * This class provides unit tests for the Block helper class, which offers
 * utility functions for handling Drupal blocks. The tests cover methods for
 * building and loading renderable arrays for blocks.
 */
class BlockTest extends UnitTestCase {

  /**
   * The mocked block manager.
   *
   * @var BlockManager|MockObject
   */
  protected BlockManager|MockObject $blockManager;

  /**
   * The mocked entity view builder interface.
   *
   * @var EntityViewBuilderInterface|MockObject
   */
  protected EntityViewBuilderInterface|MockObject $entityView;

  /**
   * The mocked entity storage interface.
   *
   * @var EntityStorageInterface|MockObject
   */
  protected EntityStorageInterface|MockObject $entityStorage;

  /**
   * The mocked entity type manager.
   *
   * @var EntityTypeManager|MockObject
   */
  protected EntityTypeManager|MockObject $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    Drupal::setContainer($container);

    // Create mock objects for the entity storage and block plugin manager.
    $this->blockManager = $this->createMock(BlockManager::class);
    $this->entityView = $this->createMock(BlockViewBuilder::class);
    $this->entityStorage = $this->createMock(EntityStorageInterface::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManager::class);
    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturn($this->entityStorage);
    $this->entityTypeManager->expects($this->any())
      ->method('getViewBuilder')
      ->willReturn($this->entityView);

    // Inject the mock objects into the Drupal container.
    $container->set('entity_type.manager', $this->entityTypeManager);
    $container->set('plugin.manager.block', $this->blockManager);
  }

  /**
   * Tests building a renderable array for a block plugin.
   */
  public function testBuildRenderableBlock(): void {
    // Create a mock block plugin object.
    $block = $this->createMock(BlockBase::class);
    $block->expects($this->once())
      ->method('build')
      ->willReturn(['#markup' => 'Mocked Block Plugin']);

    // Configure the block plugin manager mock to return the mock block plugin.
    $this->blockManager->expects($this->once())
      ->method('createInstance')
      ->with('my_block_plugin_id', [])
      ->willReturn($block);

    // Call the buildRenderable method.
    $renderable = Block::buildRenderable('my_block_plugin_id');
    $this->assertSame(['#markup' => 'Mocked Block Plugin'], $renderable);
  }

  /**
   * Tests building a renderable array for a non-existent block plugin.
   */
  public function testBuildRenderableNonExistentBlock(): void {
    // Configure the block plugin manager mock to throw an exception for a non-existent block plugin.
    $this->blockManager->expects($this->once())
      ->method('createInstance')
      ->with('non_existent_block_plugin_id', [])
      ->willThrowException(new Exception('Block plugin not found'));

    // Call the buildRenderable method for a non-existent block plugin.
    $renderable = Block::buildRenderable('non_existent_block_plugin_id');
    $this->assertSame([], $renderable);
  }

  /**
   * Tests loading a block as a renderable array.
   */
  public function testLoadRenderableBlock(): void {
    // Erstellen Sie ein Mock-Objekt für EntityInterface.
    $block = $this->createMock(BlockInterface::class);

    // Konfigurieren Sie das EntityStorage-Mock-Objekt, um das Mock-Entity-Objekt zurückzugeben.
    $this->entityStorage->expects($this->once())
      ->method('load')
      ->with('my_block_id')
      ->willReturn($block);

    // Configure the entity view builder mock to return the mock block.
    $this->entityView->expects($this->once())
      ->method('view')
      ->with($block)
      ->willReturn(['#markup' => 'Mocked Block']);

    // Call the loadRenderable method.
    $renderable = Block::loadRenderable('my_block_id');
    $this->assertSame(['#markup' => 'Mocked Block'], $renderable);
  }

  /**
   * Tests loading a non-existent block as a renderable array.
   */
  public function testLoadRenderableNonExistentBlock(): void {
    // Configure the entity storage mock to return NULL, indicating a non-existent block.
    $this->entityStorage->expects($this->once())
      ->method('load')
      ->with('non_existent_block_id')
      ->willReturn(NULL);

    // Call the loadRenderable method for a non-existent block.
    $renderable = Block::loadRenderable('non_existent_block_id');
    $this->assertSame([], $renderable);
  }

}
