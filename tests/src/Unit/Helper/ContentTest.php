<?php

/**
 * @file
 * Contains unit tests for the Content helper class in Drupal.
 *
 * This file defines the ContentTest class, which includes unit tests for
 * testing the functionality of the Content helper class. These tests ensure
 * that the Content class correctly handles text manipulation and content
 * generation.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal\codev_utils\Helper\Content;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the Content helper class.
 *
 * This class provides unit tests for the Content helper class, which offers
 * utility functions for handling content in Drupal. The tests cover methods for
 * generating blind text and substring manipulation.
 */
class ContentTest extends UnitTestCase {

  /**
   * The text to be used for testing.
   *
   * @var string
   */
  protected string $text = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.';

  /**
   * Tests Content::getBlindText method.
   */
  public function testGetBlindText() {
    $this->assertEquals(Content::NODE_BLIND_TEXT, Content::getBlindText(FALSE));
    $wrapped = sprintf('<p>%s</p>', Content::NODE_BLIND_TEXT);
    $this->assertEquals($wrapped, Content::getBlindtext());
  }

  /**
   * Tests Content::subStrByChars method for substring manipulation by
   * characters.
   */
  public function testSubStrByChars() {
    $this->assertEquals($this->text, Content::subStrByChars($this->text, 260));
    $this->assertEquals('Lorem...', Content::subStrByChars($this->text, 5));
    $this->assertEquals('Lorem...', Content::subStrByChars($this->text, 6));
    $this->assertNotEquals('Lorem...', Content::subStrByChars($this->text, 7));

    $this->assertEquals($this->text, Content::subStrByChars(
      $this->text, 260, '...', FALSE
    ));
    $this->assertEquals('Lorem...', Content::subStrByChars(
      $this->text, 5, '...', FALSE
    ));
    $this->assertEquals('Lorem ...', Content::subStrByChars(
      $this->text, 6, '...', FALSE
    ));
    $this->assertEquals('Lorem ip...', Content::subStrByChars(
      $this->text, 8, '...', FALSE
    ));
    $this->assertNotEquals('Lorem ip...', Content::subStrByChars(
      $this->text, 9, '...', FALSE
    ));
  }

  /**
   * Tests Content::subStrByWords method for substring manipulation by words.
   */
  public function testSubStrByWords() {
    $this->assertEquals('Lorem ipsum dolor...', Content::subStrByWords($this->text, 3));
    $this->assertNotEquals('Lorem ipsum dolor...', Content::subStrByWords($this->text, 4));
  }

}
