<?php

/**
 * @file
 * Contains unit tests for the Utils helper class in the Drupal Codev-IT
 * project.
 *
 * This file includes the UtilsTest class, which provides unit tests for
 * various
 * utility methods in the Utils class. These tests ensure that utility
 * functions
 * work correctly under various conditions and inputs.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal\codev_utils\Helper\Utils;
use Drupal\codev_utils_test\WeightSortTest;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the Utils helper class.
 *
 * This class provides a series of tests for the utility methods in the Utils
 * class. It tests array manipulation, value retrieval, sorting functions, and
 * more, ensuring that each utility method performs as expected.
 */
class UtilsTest extends UnitTestCase {

  /**
   * Tests Utils::mergeAttributesArray.
   *
   * Verifies that the method correctly merges multiple arrays of attributes,
   * handling special cases like 'id' attributes and ensuring uniqueness of
   * values.
   */
  public function testMergeAttributesArray() {
    $def = [
      'class' => [
        'class1',
      ],
      'data'  => [
        'data1',
      ],
    ];
    $def_id = $def + [
        'id' => 'test',
      ];

    /** @noinspection PhpUnitMisorderedAssertEqualsArgumentsInspection */
    $this->assertEquals(Utils::mergeAttributesArray($def, [
      'class' => 'FO',
      'data'  => 'data1',
    ]), [
      'class' => [
        'class1',
        'FO',
      ],
      'data'  => [
        'data1',
      ],
    ]);

    /** @noinspection PhpUnitMisorderedAssertEqualsArgumentsInspection */
    $this->assertEquals(Utils::mergeAttributesArray($def, [
      'class' => 'FO',
      'data'  => 'data2',
    ]), [
      'class' => [
        'class1',
        'FO',
      ],
      'data'  => [
        'data1',
        'data2',
      ],
    ]);

    /** @noinspection PhpUnitMisorderedAssertEqualsArgumentsInspection */
    $this->assertEquals(Utils::mergeAttributesArray($def_id, [
      'id'    => 'test2',
      'class' => 'FO',
      'data'  => 'data2',
    ]), [
      'id'    => 'test2',
      'class' => [
        'class1',
        'FO',
      ],
      'data'  => [
        'data1',
        'data2',
      ],
    ]);

    /** @noinspection PhpUnitMisorderedAssertEqualsArgumentsInspection */
    $this->assertEquals(Utils::mergeAttributesArray([
      'id'    => 'test2',
      'class' => 'FO',
      'data'  => 'data2',
    ], $def_id), [
      'id'    => 'test',
      'class' => [
        'FO',
        'class1',
      ],
      'data'  => [
        'data2',
        'data1',
      ],
    ]);
  }

  /**
   * Tests Utils::getArrayValue.
   *
   * Ensures that the method correctly retrieves values from an array based on
   * a key, and returns a fallback value if the key is not present.
   */
  public function testGetArrayValue() {
    $arr = [
      'int'    => 1,
      'string' => 'value',
      'array'  => ['item'],
    ];

    $this->assertEquals(NULL, Utils::getArrayValue('fallback', $arr));
    $this->assertTrue(Utils::getArrayValue('fallback2', $arr, TRUE));

    $this->assertEquals(1, Utils::getArrayValue('int', $arr));
    $this->assertEquals(2, Utils::getArrayValue('int_fallback', $arr, 2));

    $this->assertEquals('value', Utils::getArrayValue('string', $arr));
    $this->assertEquals('fallback', Utils::getArrayValue('string_fallback', $arr, 'fallback'));

    $this->assertEquals(['item'], Utils::getArrayValue('array', $arr));
    $this->assertEquals(['fallback'], Utils::getArrayValue('array_fallback', $arr, ['fallback']));
  }

  /**
   * Tests Utils::getArrayValueNested.
   *
   * Verifies that the method correctly retrieves values from a nested array
   * structure using dot-separated keys, returning a fallback value if the key
   * path does not exist.
   */
  public function testGetArrayValueNested() {
    $arr = [
      'nested' => [
        'int'    => 1,
        'string' => 'value',
        'array'  => ['item'],
      ],
    ];

    $this->assertEquals(NULL, Utils::getArrayValueNested('nested.fallback', $arr));
    $this->assertTrue(Utils::getArrayValueNested('nested.fallback2', $arr, TRUE));

    $this->assertEquals(1, Utils::getArrayValueNested('nested.int', $arr));
    $this->assertEquals(2, Utils::getArrayValueNested('nested.int_fallback', $arr, 2));

    $this->assertEquals('value', Utils::getArrayValueNested('nested.string', $arr));
    $this->assertEquals('fallback', Utils::getArrayValueNested('nested.string_fallback', $arr, 'fallback'));

    $this->assertEquals(['item'], Utils::getArrayValueNested('nested.array', $arr));
    $this->assertEquals(['fallback'], Utils::getArrayValueNested('nested.array_fallback', $arr, ['fallback']));
  }

  /**
   * Test: Utils::isArrayAssoc
   */
  public function testIsArrayAssoc() {
    // no associative array
    $this->assertFalse(Utils::isArrayAssoc([1, 2, 3, 4, 5]));
    $this->assertFalse(Utils::isArrayAssoc([1, 6, 4, 2, 8]));
    $this->assertFalse(Utils::isArrayAssoc(['a', 'b', 'c', 'd', 'e']));
    $this->assertFalse(Utils::isArrayAssoc(['b', 'd', 'c', 'e', 'a']));

    // associative array
    $this->assertTrue(Utils::isArrayAssoc([
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
    ]));
    $this->assertTrue(Utils::isArrayAssoc([
      'a' => 'a',
      'b' => 'b',
      'c' => 'c',
      'd' => 'd',
      'e' => 'e',
    ]));
  }

  /**
   * Test: Utils::sortArrayByKey
   */
  public function testSortArrayByKey() {
    // no index
    $sort_no_index = [
      ['weight' => 5],
      ['weight' => 2],
      ['weight' => 1],
      ['weight' => 4],
      ['weight' => 3],
    ];

    $this->assertTrue(Utils::sortArrayByKey(
      $sort_no_index, 'weight', 'desc', FALSE));

    $this->assertEquals([
      ['weight' => 1],
      ['weight' => 2],
      ['weight' => 3],
      ['weight' => 4],
      ['weight' => 5],
    ], $sort_no_index);

    $this->assertTrue(Utils::sortArrayByKey(
      $sort_no_index, 'weight', 'asc', FALSE));

    $this->assertEquals([
      ['weight' => 5],
      ['weight' => 4],
      ['weight' => 3],
      ['weight' => 2],
      ['weight' => 1],
    ], $sort_no_index);

    // keep index
    $sort_keep_index = [
      'e' => ['weight' => 5],
      'b' => ['weight' => 2],
      'a' => ['weight' => 1],
      'd' => ['weight' => 4],
      'c' => ['weight' => 3],
    ];

    $this->assertTrue(Utils::sortArrayByKey($sort_keep_index));

    $this->assertEquals([
      'a' => ['weight' => 1],
      'b' => ['weight' => 2],
      'c' => ['weight' => 3],
      'd' => ['weight' => 4],
      'e' => ['weight' => 5],
    ], $sort_keep_index);

    $this->assertTrue(Utils::sortArrayByKey($sort_keep_index, 'weight', 'asc'));

    $this->assertEquals([
      'e' => ['weight' => 5],
      'd' => ['weight' => 4],
      'c' => ['weight' => 3],
      'b' => ['weight' => 2],
      'a' => ['weight' => 1],
    ], $sort_keep_index);
  }

  /**
   * Test: Utils::sortObjectByProp
   */
  public function testSortObjectByProp() {
    // no index
    $sort_no_index = [
      new WeightSortTest(5),
      new WeightSortTest(2),
      new WeightSortTest(1),
      new WeightSortTest(4),
      new WeightSortTest(3),
    ];

    $this->assertTrue(Utils::sortObjectByProp(
      $sort_no_index, 'weight', 'desc', FALSE));

    $this->assertEquals([
      new WeightSortTest(1),
      new WeightSortTest(2),
      new WeightSortTest(3),
      new WeightSortTest(4),
      new WeightSortTest(5),
    ], $sort_no_index);

    $this->assertTrue(Utils::sortObjectByProp(
      $sort_no_index, 'weight', 'asc', FALSE));

    $this->assertEquals([
      new WeightSortTest(5),
      new WeightSortTest(4),
      new WeightSortTest(3),
      new WeightSortTest(2),
      new WeightSortTest(1),
    ], $sort_no_index);

    // keep index
    $sort_keep_index = [
      'e' => new WeightSortTest(5),
      'b' => new WeightSortTest(2),
      'a' => new WeightSortTest(1),
      'd' => new WeightSortTest(4),
      'c' => new WeightSortTest(3),
    ];

    $this->assertTrue(Utils::sortObjectByProp($sort_keep_index));

    $this->assertEquals([
      'a' => new WeightSortTest(1),
      'b' => new WeightSortTest(2),
      'c' => new WeightSortTest(3),
      'd' => new WeightSortTest(4),
      'e' => new WeightSortTest(5),
    ], $sort_keep_index);

    $this->assertTrue(Utils::sortObjectByProp($sort_keep_index, 'weight', 'asc'));

    $this->assertEquals([
      'e' => new WeightSortTest(5),
      'd' => new WeightSortTest(4),
      'c' => new WeightSortTest(3),
      'b' => new WeightSortTest(2),
      'a' => new WeightSortTest(1),
    ], $sort_keep_index);
  }

  /**
   * Test: Utils::sortObjectByMethod
   */
  public function testSortObjectByMethod() {
    // no index
    $sort_no_index = [
      new WeightSortTest(5),
      new WeightSortTest(2),
      new WeightSortTest(1),
      new WeightSortTest(4),
      new WeightSortTest(3),
    ];

    $this->assertTrue(Utils::sortObjectByMethod(
      $sort_no_index, 'getWeight', 'desc', FALSE));

    $this->assertEquals([
      new WeightSortTest(1),
      new WeightSortTest(2),
      new WeightSortTest(3),
      new WeightSortTest(4),
      new WeightSortTest(5),
    ], $sort_no_index);

    $this->assertTrue(Utils::sortObjectByMethod(
      $sort_no_index, 'getWeight', 'asc', FALSE));

    $this->assertEquals([
      new WeightSortTest(5),
      new WeightSortTest(4),
      new WeightSortTest(3),
      new WeightSortTest(2),
      new WeightSortTest(1),
    ], $sort_no_index);

    // keep index
    $sort_keep_index = [
      'e' => new WeightSortTest(5),
      'b' => new WeightSortTest(2),
      'a' => new WeightSortTest(1),
      'd' => new WeightSortTest(4),
      'c' => new WeightSortTest(3),
    ];

    $this->assertTrue(Utils::sortObjectByMethod($sort_keep_index));

    $this->assertEquals([
      'a' => new WeightSortTest(1),
      'b' => new WeightSortTest(2),
      'c' => new WeightSortTest(3),
      'd' => new WeightSortTest(4),
      'e' => new WeightSortTest(5),
    ], $sort_keep_index);

    $this->assertTrue(Utils::sortObjectByMethod($sort_keep_index, 'getWeight', 'asc'));

    $this->assertEquals([
      'e' => new WeightSortTest(5),
      'd' => new WeightSortTest(4),
      'c' => new WeightSortTest(3),
      'b' => new WeightSortTest(2),
      'a' => new WeightSortTest(1),
    ], $sort_keep_index);
  }

  /**
   * Test: Utils::strSplitValues
   */
  public function testStrParseMultipleValues() {
    $this->assertEquals([], Utils::strSplitValues(NULL));
    $this->assertEquals(['test', 'test2', 'test3'],
      Utils::strSplitValues('test test2 test3'));
    $this->assertEquals(['test', 'test2', 'test3'],
      Utils::strSplitValues('test, test2, test3'));
    $this->assertEquals(['test', 'test2', 'test3'],
      Utils::strSplitValues('test test2, test3'));
    $this->assertEquals(['test', 'test2', 'test3'],
      Utils::strSplitValues('test ,,  test2,, test3, test2'));
  }

}
