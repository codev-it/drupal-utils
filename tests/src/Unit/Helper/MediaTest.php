<?php

/**
 * @file
 * Contains unit tests for the Media helper class in the Codev-IT Drupal
 * module.
 *
 * This file defines unit tests for the Media helper class, focusing on
 * methods that generate renderable arrays for images with specific image styles
 * and attributes. It verifies the correct structure of the renderable arrays
 * and the inclusion of the specified image styles and attributes.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 */

namespace Drupal\Tests\codev_utils\Unit\Helper;

use Drupal\codev_utils\Helper\Media;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the functionality of the Media helper class.
 *
 * Provides unit tests for the Media helper class, specifically testing the
 * themeImageStyleMarkup and emptyImageMarkup methods. The tests validate
 * that the methods correctly generate renderable arrays with the specified
 * image styles and attributes, and handle empty image markup generation.
 */
class MediaTest extends UnitTestCase {

  /**
   * Tests the themeImageStyleMarkup method for standard and custom image
   * styles.
   *
   * Ensures that the method returns a renderable array with the correct
   * structure and specified image style. The test includes both standard
   * 'thumbnail' and custom 'test' image styles, along with various image
   * attributes.
   */
  public function testThemeImageStyleMarkup() {
    // Test case with 'thumbnail' image style and additional attributes.
    $expected_thumbnail = [
      '#theme'      => 'image_style',
      '#style_name' => 'thumbnail',
      '#uri'        => 'test.png',
      '#alt'        => 'Alt',
      '#title'      => 'Title',
      '#width'      => 10,
      '#height'     => 10,
      '#weight'     => 1,
    ];

    // Call the method and compare the result with the expected array.
    $result_thumbnail = Media::themeImageStyleMarkup('test.png', 'thumbnail', [
      'alt'    => 'Alt',
      'title'  => 'Title',
      'width'  => 10,
      'height' => 10,
      'weight' => 1,
    ]);
    $this->assertEquals($expected_thumbnail, $result_thumbnail);

    // Test case with a custom image style 'test'.
    $expected_custom = $expected_thumbnail;
    $expected_custom['#style_name'] = 'test';

    // Call the method for the custom style and compare the result.
    $result_custom = Media::themeImageStyleMarkup('test.png', 'test', [
      'alt'    => 'Alt',
      'title'  => 'Title',
      'width'  => 10,
      'height' => 10,
      'weight' => 1,
    ]);
    $this->assertEquals($expected_custom, $result_custom);
  }

  /**
   * Tests the emptyImageMarkup method for generating empty image markup.
   *
   * Verifies that the method correctly generates an empty image markup with
   * specified dimensions, alt text, and title. The test checks for the
   * presence of necessary keys and the correctness of their values in the
   * renderable array.
   */
  public function testEmptyImageMarkup() {
    $result = Media::emptyImageMarkup(40, 20, 'alt', 'title');

    $this->assertArrayHasKey('#theme', $result);
    $this->assertArrayHasKey('#uri', $result);
    $this->assertArrayHasKey('#width', $result);
    $this->assertArrayHasKey('#height', $result);
    $this->assertArrayHasKey('#alt', $result);
    $this->assertArrayHasKey('#title', $result);

    $this->assertEquals($result, [
      '#theme'  => 'image',
      '#uri'    => $result['#uri'],
      '#width'  => 40,
      '#height' => 20,
      '#alt'    => 'alt',
      '#title'  => 'title',
    ]);
  }

}
