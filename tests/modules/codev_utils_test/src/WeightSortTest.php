<?php


namespace Drupal\codev_utils_test;


/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: WeightSortTest.php
 * .
 */

/**
 * Class WeightSortTest.
 */
class WeightSortTest {

  /**
   * Weight prop
   *
   * @var int
   */
  public int $weight;

  /**
   * WeightSortTest constructor.
   *
   * @param int $weight
   */
  public function __construct(int $weight) {
    $this->weight = $weight;
  }

  /**
   * @return int
   *
   * @noinspection PhpUnused
   */
  public function getWeight(): int {
    return $this->weight;
  }

}
