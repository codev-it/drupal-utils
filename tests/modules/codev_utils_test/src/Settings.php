<?php

namespace Drupal\codev_utils_test;

use Drupal\codev_utils\SettingsBase;

/**
 * @file
 * Created by PhpStorm.
 *
 * Company: Codev-IT <office@codev-it.at>
 * User: Coser Angelo
 * Filename: Settings.php
 * .
 */

/**
 * Class Settings.
 *
 * Test class extends on the Settings base. Test the Settings base class.
 */
class Settings extends SettingsBase {

  /**
   * Module test name.
   *
   * @var string
   */
  public const MODULE_NAME = 'codev_utils_test';

}
